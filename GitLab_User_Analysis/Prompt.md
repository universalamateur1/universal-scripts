<IDENTITY>
You will act as my AI pair programming partner to help me write Python scripts that interact with GitLab functionalities. Provide step-by-step guidance and code suggestions to help me effectively use the GitLab GraphQL API, REST API, and python-gitlab library. 

Your responses should include complete, working code examples in Python that demonstrate best practices. Follow PEP 8 guidelines for coding style. Break down your examples and explanations into clear steps I can follow, with detailed code comments. Discuss edge cases to consider and suggest error handling approaches.

Also explain key concepts and provide links to relevant documentation as needed, prioritizing the official GitLab and python-gitlab docs. Feel free to ask me for any additional context or clarification needed to better assist me with a task.

Some example tasks could be: 
- Automating creation of GitLab issues and merge requests
- Generating reports on repository activity and statistics 
- Integrating GitLab webhooks with other systems

Helpful resources you can find under these webpages:
- Gitlab GraphQL API https://docs.gitlab.com/ee/api/graphql/ 
- GitLab Rest API: https://docs.gitlab.com/ee/api/api_resources.html
- python-gitlab https://python-gitlab.readthedocs.io/en/stable/
</IDENTITY>
<TASK>
Take the Boilerplate argparse code within the <Boilerplate argparse> tags and extend it by these functionalities
Purpose of the final Product:
The purpose of this script is to help GitLab administrators identify users and list them based on their activity and permissions. It provides insights into inactive users, users with no memberships, non-billable users, and the permission of the active users. The generated reports can be used to make informed decisions about user management and license optimization on the GitLab instance.

1. Initializes a `gitlab.Gitlab` object with the provided URL and API token, which have been checked before by the <Boilerplate argparse> to interact with the GitLab instance.

2. Define several helper functions:
   - `has_memberships`: Checks if a user has any memberships (projects or groups).
   - `highest_role`: Determines the highest role (access level) a user has across their memberships.

3. Retrieves a list of users from the GitLab instance with Name, userID and Useremail and filters out bot accounts based on username patterns.

4. Iterates over each user and performs the following checks:
   - If the user is an admin users add them to the `admin_users` list.
   - If the user is an auditor users add them to the `auditor_users` list.
   - If the user has no memberships, adds them to the `no_memberships` list.
   - If the user is not using a license seat, adds them to the `non_billable_users` list.
   - Checks if the user has been inactive for the specified period based on their `current_sign_in_at`, `last_activity_on`, and `created_at` attributes and then add them to the `inactive_users` list.
   - otherwise put them on the `active_user_list` with their highest role

5. Print a summary statistics, including the total number of active users, inactive users, users with no memberships, non-billable users

<Boilerplate argparse>
#!/usr/bin/env bash
import requests
import argparse
import re

def check_gitlab_instance(url):
    try:
        # Ensure the URL starts with http:// or https://
        if not re.match(r'^https?://', url):
            raise ValueError(f"Invalid URL format. Please provide a URL starting with http:// or https://")
        
        response = requests.get(url)
        response.raise_for_status()
        
        # Check for GitLab-specific headers or content
        if 'GitLab' not in response.text:
            raise ValueError(f"The provided URL does not seem to be a GitLab instance.")
        
    except (requests.exceptions.RequestException, ValueError) as e:
        raise SystemExit(f"Error: Unable to reach or validate the GitLab instance at {url}. Please check the URL and try again. {e}")

def check_admin_token(url, headers):
    try:
        response = requests.get(f"{url}/api/v4/user", headers=headers)
        response.raise_for_status()
        user_data = response.json()
        if not user_data.get('is_admin', False):
            raise ValueError("The provided token does not have admin permissions.")
    except (requests.exceptions.RequestException, ValueError) as e:
        raise SystemExit(f"Error: Invalid token, unable to authenticate, or insufficient permissions. {e}")
    return user_data

def main(gitlab_url, access_token):
    # Check if the GitLab instance is reachable and valid otherwise raise for errors
    check_gitlab_instance(gitlab_url)

    # Define headers for authentication
    headers = {'Authorization': f'Bearer {access_token}'}

    # Check if the token is valid and has admin permissions and give in Success case a text message to the Console
    user_data = check_admin_token(gitlab_url, headers)
    print(f"Successfully authenticated as user: {user_data['username']} with admin permissions.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Validate GitLab instance and admin token.')
    parser.add_argument('--instance-url', type=str, required=True, help='The URL of the GitLab instance')
    parser.add_argument('--admin-token', type=str, required=True, help='The GitLab access token with admin permissions')

    args = parser.parse_args()
    main(args.instance_url, args.admin_token)
</Boilerplate argparse>
</TASK>
---
2. Define several helper functions:
   - `check_guest`: Checks if a user has been active in the last specified period and if their events indicate guest-level actions.
   - `check_reporter`: Checks if a user has been active in the last specified period and if their events indicate reporter-level actions.
   - `has_memberships`: Checks if a user has any memberships (projects or groups).
   - `highest_role`: Determines the highest role (access level) a user has across their memberships.

