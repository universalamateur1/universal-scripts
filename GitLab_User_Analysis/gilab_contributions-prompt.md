# GitLab User Contribution Tracker

## Objective
Develop a robust, scalable Python script to track and analyze user contributions in GitLab projects, focusing on specific events and organizing the data in a structured format. The script should be production-ready, following best practices in software development, security, and performance optimization.

## Background
- GitLab API Documentation:
  - [Get user contribution events](https://docs.gitlab.com/ee/api/events.html#get-user-contribution-events)
  - [User contribution events](https://docs.gitlab.com/ee/user/profile/contributions_calendar.html#user-contribution-events)
  - [Target Types](https://docs.gitlab.com/ee/api/events.html#target-types)
- [GitLab API Authentication](https://docs.gitlab.com/ee/api/index.html#authentication)
- [Python Requests Library](https://docs.python-requests.org/en/latest/)
- [JSON Schema](https://json-schema.org/learn/getting-started-step-by-step.html)

## Script Requirements

### 1. Event Tracking
Capture the following events for each user:

```python
TRACKED_EVENTS = {
    'Issue': ['opened', 'closed', 'commented'],
    'MergeRequest': ['opened', 'closed', 'accepted', 'commented'],
    'WorkItem': ['opened', 'closed', 'commented'],
    'Branch': ['pushed to', 'pushed new', 'deleted']
}
```

### 2. Data Structure
Implement the following JSON schema for organizing user contributions:

```json
{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "object",
  "patternProperties": {
    "^[a-zA-Z0-9_-]+$": {
      "type": "object",
      "properties": {
        "Contributions": {
          "type": "object",
          "patternProperties": {
            "^[a-zA-Z0-9_-]+$": {
              "type": "object",
              "properties": {
                "projectName": { "type": "string" },
                "projectLink": { "type": "string", "format": "uri" },
                "Issues": {
                  "type": "object",
                  "properties": {
                    "link": { "type": "string", "format": "uri" },
                    "name": { "type": "string" },
                    "Actions": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "action": {
                            "type": "string",
                            "enum": ["commented", "opened", "closed"]
                          },
                          "timestamp": { "type": "string", "format": "date-time" },
                          "description": { "type": "string" },
                          "link": { "type": "string", "format": "uri" }
                        },
                        "required": ["action", "timestamp", "description", "link"]
                      }
                    }
                  },
                  "required": ["link", "name", "Actions"]
                },
                "Merge_Requests": {
                  "type": "object",
                  "properties": {
                    "link": { "type": "string", "format": "uri" },
                    "name": { "type": "string" },
                    "Actions": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "action": {
                            "type": "string",
                            "enum": ["commented", "opened", "accepted", "closed"]
                          },
                          "timestamp": { "type": "string", "format": "date-time" },
                          "description": { "type": "string" },
                          "link": { "type": "string", "format": "uri" }
                        },
                        "required": ["action", "timestamp", "description", "link"]
                      }
                    }
                  },
                  "required": ["link", "name", "Actions"]
                },
                "WorkItems": {
                  "type": "object",
                  "properties": {
                    "link": { "type": "string", "format": "uri" },
                    "name": { "type": "string" },
                    "Actions": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "action": {
                            "type": "string",
                            "enum": ["commented", "opened", "closed"]
                          },
                          "timestamp": { "type": "string", "format": "date-time" },
                          "description": { "type": "string" },
                          "link": { "type": "string", "format": "uri" }
                        },
                        "required": ["action", "timestamp", "description", "link"]
                      }
                    }
                  },
                  "required": ["link", "name", "Actions"]
                },
                "Branches": {
                  "type": "object",
                  "properties": {
                    "link": { "type": "string", "format": "uri" },
                    "name": { "type": "string" },
                    "Actions": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "action": {
                            "type": "string",
                            "enum": ["pushed to", "pushed new", "deleted"]
                          },
                          "timestamp": { "type": "string", "format": "date-time" },
                          "description": { "type": "string" },
                          "link": { "type": "string", "format": "uri" }
                        },
                        "required": ["action", "timestamp", "description", "link"]
                      }
                    }
                  },
                  "required": ["link", "name", "Actions"]
                }
              },
              "required": ["projectName", "projectLink", "Issues", "Merge_Requests", "WorkItems", "Branches"],
              "additionalProperties": false
            }
          },
          "additionalProperties": false
        }
      },
      "required": ["Contributions"],
      "additionalProperties": false
    }
  },
  "additionalProperties": false
}
```

### 3. Script Implementation
Review the attached `gitlab_contributions.py` script.
Implement the `gitlab_contributions.py` script with the following components and features:

1. API Interaction:
   - Use the `requests` library for making API calls.
   - Implement proper authentication using GitLab Personal Access Tokens.
   - Handle rate limiting with exponential backoff and jitter.
   - Implement pagination to handle large datasets efficiently.

2. Data Processing:
   - Filter events based on the `TRACKED_EVENTS` dictionary.
   - Implement data validation using the JSON schema.
   - Use type hints throughout the code for improved readability and error catching.

3. Error Handling and Logging:
   - Implement comprehensive error handling for API requests, data processing, and file operations.
   - Use the `logging` module to create structured logs with different severity levels.
   - Implement a custom exception hierarchy for more granular error handling.

4. Performance Optimization:
   - Use asynchronous programming (e.g., `asyncio` and `aiohttp`) for concurrent API requests.
   - Implement caching mechanisms to store frequently accessed data and reduce API calls.
   - Use generator expressions and itertools for memory-efficient data processing.

5. Security:
   - Store API tokens securely using environment variables or a secure credential manager.
   - Implement input sanitization to prevent injection attacks.
   - Use HTTPS for all API communications.

6. Code Organization:
   - Follow the PEP 8 style guide for code formatting.
   - Use meaningful variable and function names.
   - Organize code into logical modules and classes.

7. Scalability Considerations:
   - Implement parallel processing for handling multiple users or projects simultaneously.
   - Use streaming parsers (e.g., `ijson`) for handling large JSON responses.

8. Command-line Interface:
   - Implement a user-friendly CLI using `argparse`.
   - Allow users to specify input parameters such as GitLab instance URL, user(s) to track, date range, etc.

9. Output Formats:
   - Provide options to output data in JSON, CSV, and human-readable formats.
   - Implement pretty printing for console output.

10. Documentation:
    - Include comprehensive docstrings for all functions, classes, and modules.
    - Provide inline comments for complex logic.

## Example Usage
The script should be runnable from the command line with various options:

```bash
python3 gitlab_contributions.py [--url URL] [--token TOKEN] --users USER1 [USER2 ...] [--days DAYS] [--output FILENAME]
```

```bash
python gitlab_contributions.py --url https://gitlab.com --token YOUR_TOKEN --users john,jane --days 30 --output contributions.json
```

## Example Output
```json
{
  "JohnDoe": {
    "Contributions": {
      "Project1": {
        "projectName": "Amazing Project",
        "projectLink": "https://gitlab.com/amazing-project",
        "Issues": {
          "link": "https://gitlab.com/amazing-project/-/issues",
          "name": "Project Issues",
          "Actions": [
            {
              "action": "commented",
              "timestamp": "2023-05-15T14:30:00Z",
              "description": "Added a comment suggesting a new approach to the problem.",
              "link": "https://gitlab.com/amazing-project/-/issues/42#note_123456"
            },
            {
              "action": "opened",
              "timestamp": "2023-05-14T09:00:00Z",
              "description": "Opened a new issue to track the refactoring of the authentication module.",
              "link": "https://gitlab.com/amazing-project/-/issues/43"
            }
          ]
        },
        "Merge_Requests": {
          "link": "https://gitlab.com/amazing-project/-/merge_requests",
          "name": "Project Merge Requests",
          "Actions": [
            {
              "action": "accepted",
              "timestamp": "2023-05-16T11:45:00Z",
              "description": "Accepted the merge request after reviewing the changes and running tests.",
              "link": "https://gitlab.com/amazing-project/-/merge_requests/15"
            }
          ]
        },
        "WorkItems": {
          "link": "https://gitlab.com/amazing-project/-/work_items",
          "name": "Project Work Items",
          "Actions": []
        },
        "Branches": {
          "link": "https://gitlab.com/amazing-project/-/branches",
          "name": "Project Branches",
          "Actions": [
            {
              "action": "pushed to",
              "timestamp": "2023-05-15T16:20:00Z",
              "description": "Pushed updates to the 'feature/new-ui' branch, including responsive design improvements.",
              "link": "https://gitlab.com/amazing-project/-/tree/feature/new-ui"
            }
          ]
        }
      }
    }
  }
}
```

## Deliverables
1. `gitlab_contributions.py` script
2. README.md file with usage instructions and any dependencies
3. Sample output JSON file

## Additional Resources
- [Python Performance Optimization](https://wiki.python.org/moin/PythonSpeed/PerformanceTips)
- [OWASP Security Practices](https://owasp.org/www-project-top-ten/)
- [Python Logging Best Practices](https://docs.python.org/3/howto/logging.html)
- [Asynchronous Programming in Python](https://docs.python.org/3/library/asyncio.html)
- [Python Type Hinting](https://docs.python.org/3/library/typing.html)

Please implement the `gitlab_contributions.py` script according to these requirements, focusing on creating a high-quality, efficient, and secure Python script. Ensure that the script is well-documented with inline comments and docstrings, and follows best practices for Python development.

def calculate_total_price(items, discount_percentage):
    subtotal = sum(item['price'] for item in items)
    discount = subtotal * (discount_percentage / 100)
    total = subtotal - discount
    return total
