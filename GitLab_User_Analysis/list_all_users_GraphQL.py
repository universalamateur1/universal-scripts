#!/usr/bin/env bash
import gitlab
import requests
import argparse
import re

def check_gitlab_instance(url):
    """
    Check if the provided URL is a reachable and valid GitLab instance.

    Args: url (str): The URL of the GitLab instance to check.

    Returns: str: The validated URL of the GitLab instance.

    Raises: SystemExit: If the provided URL is invalid, unreachable, or not a GitLab instance.
    """

    try:
        # Ensure Url ends with /
        if not url.endswith('/'):
            url += '/'
        # Ensure the URL starts with http:// or https://
        if not re.match(r'^https?://', url):
            raise ValueError(f"Invalid URL format. Please provide a URL starting with http:// or https://")
        # Ensure the URL is reachable
        response = requests.get(url)
        response.raise_for_status()
        # Check for GitLab-specific headers or content
        if 'GitLab' not in response.text:
            raise ValueError(f"The provided URL does not seem to be a GitLab instance.")
    # Raise for errors
    except (requests.exceptions.RequestException, ValueError) as e:
        raise SystemExit(f"Error: Unable to reach or validate the GitLab instance at {url}. Please check the URL and try again. {e}")
    return url

def check_admin_token(url, headers):
    """
    Check if the provided access token has admin permissions on the GitLab instance.

    Args: url (str): The URL of the GitLab instance. 
    headers (dict): The headers containing the access token for authentication.

    Returns: dict: The user data of the authenticated admin user.

    Raises: SystemExit: If the provided token is invalid, unable to authenticate, or lacks admin permissions.
    """
    try:
        response = requests.get(f"{url}/api/v4/user", headers=headers)
        response.raise_for_status()
        user_data = response.json()
        if not user_data.get('is_admin', False):
            raise ValueError("The provided token does not have admin permissions.")
    except (requests.exceptions.RequestException, ValueError) as e:
        raise SystemExit(f"Error: Invalid token, unable to authenticate, or insufficient permissions. {e}")
    return user_data

def main(gitlab_url, access_token):
    """
    The main function to validate the GitLab instance and admin token.
    Then call the GraphQL Endpoint to list all Instance USers

    Args: 
        gitlab_url (str): The URL of the GitLab instance.
        access_token (str): The GitLab access token with admin permissions.
    """
    # Check if the GitLab instance is reachable and valid otherwise raise for errors
    checked_gitlab_url = check_gitlab_instance(gitlab_url)

    # Define headers for authentication
    headers_rest = {'Authorization': f'Bearer {access_token}'}
    # Check if the token is valid and has admin permissions and give in Success case a text message to the Console
    user_data = check_admin_token(checked_gitlab_url, headers_rest)
    print(f"Successfully authenticated as user: {user_data['username']} with admin permissions.")

    # GitLab GraphQL API endpoint
    url_graph = checked_gitlab_url + 'api/graphql'
    # Set the headers, including the private token for authentication with GraphQL
    headers_graph = {
    'Content-Type': 'application/json',
    'Authorization': f'Bearer {access_token}'
    }

    # GraphQL query to retrieve users with pagination
    query = '''
    query getUsers($after: String, $first: Int!) {
    users(active: true, blocked: false, bot: false, after: $after, first: $first) {
        pageInfo {
        endCursor
        hasNextPage
        }
        nodes {
        id
        username
        email
        name
        state
        avatarUrl
        webUrl
        createdAt
        lastActivityOn
        }
    }
    }
    '''

    # Variables for pagination
    variables = {
        'first': 100,  # Number of users to retrieve per page
        'after': None  # Cursor for pagination
    }

    # List to store all users
    all_users = []

    while True:
    # Send a POST request to the GitLab GraphQL API
        response = requests.post(url_graph, json={'query': query, 'variables': variables}, headers=headers_graph)
        
        # Check if the request was successful
        if response.status_code == 200:
            result = response.json()
            
            # Extract the user nodes from the response
            users = result['data']['users']['nodes']
            all_users.extend(users)
            
            # Check if there are more pages
            page_info = result['data']['users']['pageInfo']
            if not page_info['hasNextPage']:
                break
            
            # Set the cursor for the next page
            variables['after'] = page_info['endCursor']
        else:
            print(f"Error: {response.status_code} - {response.text}")
            break

    # Print the total number of users
    print(f"Total users: {len(all_users)}")

    # Print the user details
    for user in all_users:
        print(f"User ID: {user['id']}, Username: {user['username']}, Email: {user['email']}")

if __name__ == "__main__":
    # Parse the command line arguments
    parser = argparse.ArgumentParser(description='Validate GitLab instance and admin token.')
    # Required arguments
    parser.add_argument('--instance-url', type=str, required=True, help='The URL of the GitLab instance')
    parser.add_argument('--admin-token', type=str, required=True, help='The GitLab access token with admin permissions')

    # Parse the arguments to main function
    args = parser.parse_args()
    main(args.instance_url, args.admin_token)