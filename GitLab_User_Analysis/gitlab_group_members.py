import argparse
import csv
import json
import re
from datetime import datetime
from urllib.parse import urlparse

import requests

def validate_gitlab_url(url):
    """
    Validate and normalize the GitLab group URL.
    
    Args:
    url (str): The input URL to validate
    
    Returns:
    str: The normalized GitLab group URL
    
    Raises:
    ValueError: If the URL format is invalid
    """
    patterns = [
        r'^https?://gitlab\.com/(?P<group>.+?)/?$',
        r'^gitlab\.com/(?P<group>.+?)/?$',
        r'^/(?P<group>.+?)/?$',
        r'^(?P<group>.+?)/?$'
    ]
    
    for pattern in patterns:
        match = re.match(pattern, url)
        if match:
            group = match.group('group')
            return f"https://gitlab.com/{group}"
    
    raise ValueError("Invalid GitLab group URL format")

def validate_token(token):
    """
    Validate the GitLab API token.
    
    Args:
    token (str): The GitLab API token to validate
    
    Raises:
    ValueError: If the token is invalid
    """
    url = "https://gitlab.com/api/v4/personal_access_tokens/self"
    headers = {"Authorization": f"Bearer {token}"}
    response = requests.get(url, headers=headers)
    
    print(f"Testing the Token")  # Debug print
    if response.status_code != 200:
        print(f"Token validation response: {response}")  # Debug print
        raise ValueError("Invalid GitLab API token")
    else:
        print(f"Received valid GitLab.com token")  # Debug print

def check_group_access(url, token):
    """
    Check if the token has access to the given group.
    
    Args:
    url (str): The full URL of the GitLab group
    token (str): The GitLab API token
    
    Raises:
    ValueError: If the token has no access to the group
    """
    group_path = urlparse(url).path.strip('/')
    api_url = f"https://gitlab.com/api/v4/groups/{group_path}"
    headers = {"Authorization": f"Bearer {token}"}
    params = {
        "with_custom_attributes": "true",
        "with_projects": "false"
    }
    
    response = requests.get(api_url, headers=headers, params=params)
    
    print(f"Checking group access for: {group_path}")  # Debug print
    print(f"API response status code: {response.status_code}")  # Debug print
    
    if response.status_code == 404:
        raise ValueError("Token has no access to the specified group")
    elif response.status_code != 200:
        raise ValueError(f"Failed to check group access: HTTP {response.status_code}")
    
    print("Group access confirmed")  # Debug print

def get_group_members(url, token):
    """
    Fetch group members using the GitLab GraphQL API.
    
    Args:
    url (str): The full URL of the GitLab group
    token (str): The GitLab API token
    
    Returns:
    dict: The JSON response from the GraphQL API
    
    Raises:
    Exception: If the GraphQL query fails
    """
    group_path = urlparse(url).path.strip('/')
    graphql_url = "https://gitlab.com/api/graphql"
    headers = {"Authorization": f"Bearer {token}"}
    
    query = """
    query ($fullPath: ID!) {
      group(fullPath: $fullPath) {
        groupMembersCount
        fullPath
        webUrl
        groupMembers(relations: [DIRECT]) {
          pageInfo {
            endCursor
            startCursor
            hasNextPage
          }
          nodes {
            user {
              username
              name
              bot
              human
              state
              lastActivityOn
              commitEmail
              publicEmail
            }
          }
        }
      }
    }
    """
    
    variables = {"fullPath": group_path}
    response = requests.post(graphql_url, json={"query": query, "variables": variables}, headers=headers)
    
    print(f"GraphQL API response status code: {response.status_code}")  # Debug print
    
    if response.status_code != 200:
        raise Exception(f"GraphQL query failed with status code {response.status_code}")
    
    return response.json()

def main():
    parser = argparse.ArgumentParser(description="Fetch GitLab group members and save to CSV")
    parser.add_argument("--url", required=True, help="Full web URL of the GitLab group")
    parser.add_argument("--token", required=True, help="Valid GitLab API token")
    args = parser.parse_args()

    try:
        validated_url = validate_gitlab_url(args.url)
        print(f"Validated group URL: {validated_url}")  # Debug print
        
        validate_token(args.token)
        print("Token validated successfully")  # Debug print
        
        check_group_access(validated_url, args.token)
        
        try:
            result = get_group_members(validated_url, args.token)
        except Exception as e:
            print(f"Failed to fetch group members: {str(e)}")  # Debug print
            print("This might be due to insufficient permissions. Proceeding with limited information.")
            result = {"data": {"group": {"fullPath": urlparse(validated_url).path.strip('/'), "groupMembersCount": "Unknown"}}}
        
        group_data = result['data']['group']
        
        if 'groupMembers' in group_data:
            all_members = group_data['groupMembers']['nodes']
            human_members = [node['user'] for node in all_members if node['user']['human'] and node['user']['state'] == 'active']
            non_human_members = [node['user'] for node in all_members if not node['user']['human'] or node['user']['state'] != 'active']
            members_with_email = [user for user in human_members if user['publicEmail'] or user['commitEmail']]
            members_without_email = [user for user in human_members if not user['publicEmail'] and not user['commitEmail']]
        else:
            human_members = []
            non_human_members = []
            members_with_email = []
            members_without_email = []
            print("Unable to fetch group members. This might be due to insufficient permissions.")  # Debug print
        
        timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M")
        filename = f"{group_data['fullPath']}-{timestamp}.csv"
        
        with open(filename, 'w', newline='') as csvfile:
            fieldnames = ["username", "name", "lastActivityOn", "commitEmail", "publicEmail"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for member in human_members:
                writer.writerow({
                    "username": member['username'],
                    "name": member['name'],
                    "lastActivityOn": member['lastActivityOn'],
                    "commitEmail": member['commitEmail'] or '',
                    "publicEmail": member['publicEmail'] or ''
                })
        
        print(f"\nGroup Statistics:")
        print(f"Total number of group users: {len(all_members)}")
        print(f"Number of non-human/bot users: {len(non_human_members)}")
        print(f"Number of human users: {len(human_members)}")
        print(f"Number of users without a public email: {len(members_without_email)}")
        print(f"Number of users with an email: {len(members_with_email)}")
        
        print(f"\nCSV file '{filename}' has been created successfully.")  # Debug print
    
    except ValueError as e:
        print(f"Error: {str(e)}")
    except Exception as e:
        print(f"An unexpected error occurred: {str(e)}")

if __name__ == "__main__":
    main()
