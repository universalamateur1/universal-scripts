# GitLab User Contributions Analyzer

## Overview

This Python script analyzes and retrieves user contributions from GitLab. It can work with both GitLab.com and self-hosted GitLab instances. The script fetches user contribution events for specified users over a given time period and provides both a summary output and detailed JSON data files.

## Features

- Connects to GitLab.com or self-hosted GitLab instances
- Supports both authenticated and unauthenticated access (with limitations for unauthenticated)
- Retrieves user contribution events for specified users
- Allows specifying the number of days to fetch contributions for
- Provides a summary of contributions in the console output
- Generates detailed JSON files with raw contribution data

## Requirements

- Python 3.6+
- `gitlab` library
- `requests` library

## Usage

```
python3 gitlab_contributions.py [--url URL] [--token TOKEN] --users USER1 [USER2 ...] [--days DAYS]
```

### Arguments

- `--url`: GitLab server URL (default: https://gitlab.com)
- `--token`: Personal Access Token (optional for gitlab.com, required for self-hosted)
- `--users`: List of GitLab usernames to analyze (required)
- `--days`: Number of days to fetch contributions for (default: 30)

## Output

1. Console output: Provides a summary of contributions for each user.
2. JSON files: Detailed raw contribution data for each user, saved in the `output` directory.

## Key Functions

1. `parse_arguments()`: Handles command-line argument parsing.
2. `validate_gitlab_connection()`: Validates the GitLab server connection and authentication.
3. `validate_users()`: Checks if the provided usernames exist on the GitLab server.
4. `get_user_contributions()`: Fetches contribution events for a given user.
5. `display_contributions()`: Displays a summary of user contributions in the console.
6. `display_contributions_raw()`: Generates detailed JSON files with raw contribution data.

## Notes

- Authenticated access provides more comprehensive results.
- Unauthenticated access is limited to public data on GitLab.com.
- The script creates an 'output' directory to store JSON files if it doesn't exist.

## Research links

- [Events API - Get user contribution events](https://docs.gitlab.com/ee/api/events.html#get-user-contribution-events)
- [GitLab - Contributions calendar](https://docs.gitlab.com/ee/user/profile/contributions_calendar.html)
- [Events API - Target Types](https://docs.gitlab.com/ee/api/events.html#target-types)
- [User contribution events](https://docs.gitlab.com/ee/user/profile/contributions_calendar.html#user-contribution-events)
- Represents the contributions of a user. [GraphQl ContributionAnalyticsContribution](https://docs.gitlab.com/ee/api/graphql/reference/#contributionanalyticscontribution)
- Provides the aggregated contributions by users within the group and its subgroups [GraphQl Group\.contributions](https://docs.gitlab.com/ee/api/graphql/reference/#groupcontributions)
- PArt of GitLab Code that fills the Contribution Calendar: [events\_finder\.rb 3\.52 KiB](https://gitlab.com/gitlab-org/gitlab/-/blob/4f9a03e14fd55fdb4bf04c4018bdf6b2e04a4210/app/finders/events_finder.rb#L54)

### Contributing Events to capture

### User activity complete

- `created`, `closed`, or `reopened` a work item (__Epic__, Issue, Work Item)
- `commented` on a work item
- `approved` a merge request
- `merged` a merge request
- `pushed` commits
- `commented` on a merge request
- `updated` design or wiki page
- `destroyed` design, milestone or wiki page

### User activity First MVC

- `opened` an Issue
- `opened` a Work Item
- `opened` a merge request
- `closed` an Issue
- `closed` a Work Item
- `closed` a merge request
- `accpeted` a merge request
- `commented` on an Issue
- `commented` on a Work Item
- `commented` on a merge request
- `commented` on a diff in a merge request
- `pushed to` commits to a branch
- `pushed new` commits to a branch and created the branch
- `deleted` a branch

## Data research

The Different Actions taken (RegEx `"action_name": "(.+)",`):
"action_name": "commented on","pushed to","opened","pushed new","closed","deleted","accepted"

Actions with regex `"action_name":\s"(.+)"[\s\S]*?"target_type":\s(.+),`

### commented on

#### commented on - DiffNote

"action_name": "commented on",
"target_type": "DiffNote",

#### commented on - Note

"action_name": "commented on",
"target_type": "Note",

### opened

#### opened - MergeRequest

"action_name": "opened",
"target_type": "MergeRequest",

#### opened - Issue

"action_name": "opened",
"target_type": "Issue",

#### opened - WorkItem

"action_name": "opened",
"target_type": "WorkItem",

### accepted

#### accepted - MergeRequest

"action_name": "accepted",
"target_type": "MergeRequest",

### closed

#### closed - MergeRequest

"action_name": "closed",
"target_type": "MergeRequest",

#### closed - Issue

"action_name": "closed",
"target_type": "Issue",

#### closed - WorkItem

"action_name": "closed",
"target_type": "WorkItem",

### pushed to

#### pushed to - null

"action_name": "pushed to",
"target_type": null,
"push_data":"commit_count": INTEGER,
"push_data":"action": "pushed",
"push_data":"ref_type": "branch",

#### pushed new - null

"action_name": "pushed new",
"target_type": null,
"push_data":"commit_count": INTEGER,
"push_data":"action": "created",
"push_data":"ref_type": "branch",

### deleted

#### deleted - null

"action_name": "deleted",
"target_type": null,
"push_data":"action": "removed",
"push_data":"ref_type": "branch",

#### Prompt

<PROMPT>
Step 1. Research these GitLab Documentation links about how to Get user contribution events https://docs.gitlab.com/ee/api/events.html#get-user-contribution-events and  User contribution events for available types for the action parameter https://docs.gitlab.com/ee/user/profile/contributions_calendar.html#user-contribution-events as well as the Target Types https://docs.gitlab.com/ee/api/events.html#target-types
Step 2. Review this list of Events in the <list_of_events> tags I want to capture with the script per user, the "action_name","target_type" and other differentiator for the specific actions and what I want to show in the output.
Step 3. Review the gitlab_contributions.py script attached to our conversation and adjust it so that these events are found pe user and the requested data is captures in the form I define in the <data_sctructure> and <data_sctructure_Description> tags. You find a ready example in the <data_set_example> tags.

<list_of_events>
- `opened` an Issue
- `opened` a Work Item
- `opened` a merge request
- `closed` an Issue
- `closed` a Work Item
- `closed` a merge request
- `accpeted` a merge request
- `commented` on an Issue
- `commented` on a Work Item
- `commented` on a merge request
- `commented` on a diff in a merge request
- `pushed to` commits to a branch
- `pushed new` commits to a branch and created the branch
- `deleted` a branch
</list_of_events>
<data_structure>
{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "object",
  "patternProperties": {
    "^[a-zA-Z0-9_-]+$": {
      "type": "object",
      "properties": {
        "Contributions": {
          "type": "object",
          "patternProperties": {
            "^[a-zA-Z0-9_-]+$": {
              "type": "object",
              "properties": {
                "projectName": {
                  "type": "string"
                },
                "projectLink": {
                  "type": "string",
                  "format": "uri"
                },
                "Issues": {
                  "type": "object",
                  "properties": {
                    "link": {
                      "type": "string",
                      "format": "uri"
                    },
                    "name": {
                      "type": "string"
                    },
                    "Actions": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "action": {
                            "type": "string",
                            "enum": ["commented on", "opened", "closed"]
                          },
                          "timestamp": {
                            "type": "string",
                            "format": "date-time"
                          },
                          "description": {
                            "type": "string"
                          },
                          "link": {
                            "type": "string",
                            "format": "uri"
                          }
                        },
                        "required": ["action", "timestamp", "description", "link"]
                      }
                    }
                  },
                  "required": ["link", "name", "Actions"]
                },
                "Merge_Requests": {
                  "type": "object",
                  "properties": {
                    "link": {
                      "type": "string",
                      "format": "uri"
                    },
                    "name": {
                      "type": "string"
                    },
                    "Actions": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "action": {
                            "type": "string",
                            "enum": ["commented on", "opened", "accepted", "closed"]
                          },
                          "timestamp": {
                            "type": "string",
                            "format": "date-time"
                          },
                          "description": {
                            "type": "string"
                          },
                          "link": {
                            "type": "string",
                            "format": "uri"
                          }
                        },
                        "required": ["action", "timestamp", "description", "link"]
                      }
                    }
                  },
                  "required": ["link", "name", "Actions"]
                },
                "WorkItems": {
                  "type": "object",
                  "properties": {
                    "link": {
                      "type": "string",
                      "format": "uri"
                    },
                    "name": {
                      "type": "string"
                    },
                    "Actions": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "action": {
                            "type": "string",
                            "enum": ["commented on", "opened", "closed"]
                          },
                          "timestamp": {
                            "type": "string",
                            "format": "date-time"
                          },
                          "description": {
                            "type": "string"
                          },
                          "link": {
                            "type": "string",
                            "format": "uri"
                          }
                        },
                        "required": ["action", "timestamp", "description", "link"]
                      }
                    }
                  },
                  "required": ["link", "name", "Actions"]
                },
                "Branches": {
                  "type": "object",
                  "properties": {
                    "link": {
                      "type": "string",
                      "format": "uri"
                    },
                    "name": {
                      "type": "string"
                    },
                    "Actions": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "action": {
                            "type": "string",
                            "enum": ["pushed to", "pushed new", "deleted"]
                          },
                          "timestamp": {
                            "type": "string",
                            "format": "date-time"
                          },
                          "description": {
                            "type": "string"
                          },
                          "link": {
                            "type": "string",
                            "format": "uri"
                          }
                        },
                        "required": ["action", "timestamp", "description", "link"]
                      }
                    }
                  },
                  "required": ["link", "name", "Actions"]
                }
              },
              "required": ["projectName", "projectLink", "Issues", "Merge_Requests", "WorkItems", "Branches"],
              "additionalProperties": false
            }
          },
          "additionalProperties": false
        }
      },
      "required": ["Contributions"],
      "additionalProperties": false
    }
  },
  "additionalProperties": false
}
</data_structure>

<data_structure_description>
1. Root Level:
   - The root is an object where keys are GitLab usernames.
   - Each username key contains a "Contributions" object.

2. Contributions Object:
   - Contains multiple project objects, each keyed by a project identifier.

3. Project Object:
   - Properties:
     - "projectName": String representing the name of the project.
     - "projectLink": URI string linking to the project.
     - Four main contribution categories: "Issues", "Merge_Requests", "WorkItems", and "Branches".

4. Contribution Category Objects (Issues, Merge_Requests, WorkItems, Branches):
   - Each has the following properties:
     - "link": URI string to the category's GitLab page.
     - "name": String name of the category.
     - "Actions": Array of action objects.

5. Action Object:
   - Properties:
     - "action": String enum of possible actions (varies by category).
     - "timestamp": ISO 8601 date-time string.
     - "description": String describing the action taken.
     - "link": URI string to the specific object (issue, merge request, etc.) acted upon.

6. Action Types by Category:
   - Issues: "commented on", "opened", "closed"
   - Merge_Requests: "commented on", "opened", "accepted", "closed"
   - WorkItems: "commented on", "opened", "closed"
   - Branches: "pushed to", "pushed new", "deleted"

Usage Guidelines for Programming:
1. Access user data using the GitLab username as the top-level key.
2. Iterate through the "Contributions" object keys to access individual projects.
3. Use "projectName" and "projectLink" for project-level information.
4. Access contribution categories ("Issues", "Merge_Requests", "WorkItems", "Branches") directly under each project.
5. Use the "link" field in each category for the GitLab URL and "name" for the display name.
6. Process actions by iterating through the "Actions" array in each category.
7. Use the "action" field to filter for specific types of contributions.
8. Parse the "timestamp" field for chronological processing or filtering.
9. Use the "link" field in each action for direct access to the specific GitLab object.
10. Implement error handling by checking for the existence of keys before accessing nested properties.

This schema allows for efficient querying and aggregation of user contributions across multiple projects and contribution types in GitLab, with direct links to each contributed object.
</data_structure_description>

<data_set_example>
{
  "JohnDoe": {
    "Contributions": {
      "Project1": {
        "projectName": "Amazing Project",
        "projectLink": "https://gitlab.com/amazing-project",
        "Issues": {
          "link": "https://gitlab.com/amazing-project/-/issues",
          "name": "Project Issues",
          "Actions": [
            {
              "action": "commented on",
              "timestamp": "2023-05-15T14:30:00Z",
              "description": "Added a comment suggesting a new approach to the problem.",
              "link": "https://gitlab.com/amazing-project/-/issues/42#note_123456"
            },
            {
              "action": "opened",
              "timestamp": "2023-05-14T09:00:00Z",
              "description": "Opened a new issue to track the refactoring of the authentication module.",
              "link": "https://gitlab.com/amazing-project/-/issues/43"
            }
          ]
        },
        "Merge_Requests": {
          "link": "https://gitlab.com/amazing-project/-/merge_requests",
          "name": "Project Merge Requests",
          "Actions": [
            {
              "action": "accepted",
              "timestamp": "2023-05-16T11:45:00Z",
              "description": "Accepted the merge request after reviewing the changes and running tests.",
              "link": "https://gitlab.com/amazing-project/-/merge_requests/15"
            }
          ]
        },
        "WorkItems": {
          "link": "https://gitlab.com/amazing-project/-/work_items",
          "name": "Project Work Items",
          "Actions": []
        },
        "Branches": {
          "link": "https://gitlab.com/amazing-project/-/branches",
          "name": "Project Branches",
          "Actions": [
            {
              "action": "pushed to",
              "timestamp": "2023-05-15T16:20:00Z",
              "description": "Pushed updates to the 'feature/new-ui' branch, including responsive design improvements.",
              "link": "https://gitlab.com/amazing-project/-/tree/feature/new-ui"
            }
          ]
        }
      }
    }
  }
}
</data_set_example>
</PROMPT>
