#!/usr/bin/env python3

"""
GitLab SaaS Namespace User Activity Analyzer

This script connects to GitLab.com, retrieves user activity information for a specific
namespace via GraphQL API, and generates a CSV report of users sorted by their last
activity date.

Usage:
    python GitLab_Com_user_list_by_activity.py --namespace "group/subgroup" --token "your-token"
    
Environment variables:
    GITLAB_PRIVATE_TOKEN: Can be used to store the GitLab token
"""

import argparse
import csv
import json
import re
import requests
import sys
from datetime import datetime
from urllib.parse import urlparse, quote

def validate_namespace_path(namespace):
    """
    Validate and normalize the GitLab namespace path.
    
    Args:
        namespace (str): The input namespace to validate
    
    Returns:
        str: The normalized namespace path
    
    Raises:
        ValueError: If the namespace format is invalid
    """
    # Remove any GitLab.com URL parts if provided
    namespace = namespace.replace('https://gitlab.com/', '')
    namespace = namespace.replace('gitlab.com/', '')
    namespace = namespace.strip('/')
    
    # Validate namespace format
    if not re.match(r'^[a-zA-Z0-9_/-]+$', namespace):
        raise ValueError("Invalid namespace format. Use format: group/subgroup")
    
    return namespace

def validate_token(token):
    """
    Validate if the provided token has sufficient permissions.
    
    Args:
        token (str): The GitLab API token to validate
    
    Returns:
        bool: True if token is valid
    
    Raises:
        ValueError: If token is invalid or lacks required permissions
    """
    print("[DEBUG] Validating token permissions...")
    
    headers = {'Authorization': f'Bearer {token}'}
    
    try:
        response = requests.get(
            "https://gitlab.com/api/v4/user",
            headers=headers,
            timeout=10
        )
        if response.status_code != 200:
            raise ValueError("Invalid token or insufficient permissions")
        return True
    except requests.exceptions.RequestException as e:
        raise ValueError(f"Failed to validate token: {str(e)}")
def execute_graphql_query(token, namespace, cursor=None):
    """
    Execute the GraphQL query to get group member data with pagination.
    
    Args:
        token (str): The GitLab API token
        namespace (str): The group path
        cursor (str): Pagination cursor
    
    Returns:
        dict: Query results
    
    Raises:
        ValueError: If the query fails
    """
    print(f"[DEBUG] Executing GraphQL query{' with cursor: ' + cursor if cursor else ''}...")
    
    headers = {
        'Authorization': f'Bearer {token}',
        'Content-Type': 'application/json',
    }
    
    query = """
    query GetGroupMembers($fullPath: ID!, $after: String) {
      group(fullPath: $fullPath) {
        groupMembersCount
        fullPath
        webUrl
        groupMembers(
          relations: [DIRECT],
          first: 100,
          after: $after
        ) {
          pageInfo {
            endCursor
            hasNextPage
          }
          nodes {
            user {
              id
              username
              name
              publicEmail
              lastActivityOn
              state
              bot
              human
              commitEmail
              emails {
                nodes {
                  email
                }
              }
            }
          }
        }
      }
    }
    """
    
    variables = {
        "fullPath": namespace,
        "after": cursor
    }
    
    try:
        response = requests.post(
            "https://gitlab.com/api/graphql",
            headers=headers,
            json={'query': query, 'variables': variables},
            timeout=30
        )
        
        if response.status_code != 200:
            print(f"[ERROR] Query failed with status code: {response.status_code}")
            print(f"[ERROR] Response: {response.text}")
            raise ValueError(f"GraphQL query failed with status code: {response.status_code}")
        
        result = response.json()
        
        if 'errors' in result:
            print("[ERROR] GraphQL Query Errors:")
            for error in result['errors']:
                print(f"  {error.get('message')}")
            raise ValueError("GraphQL query returned errors")
            
        return result
        
    except requests.exceptions.RequestException as e:
        raise ValueError(f"Failed to execute GraphQL query: {str(e)}")

def get_all_users(token, namespace):
    """
    Retrieve all users from the group using pagination.
    
    Args:
        token (str): The GitLab API token
        namespace (str): The group path
    
    Returns:
        list: List of user data dictionaries
    """
    print(f"[DEBUG] Retrieving all users for group: {namespace}")
    
    all_users = []
    cursor = None
    page = 1
    
    while True:
        result = execute_graphql_query(token, namespace, cursor)
        
        group_data = result.get('data', {}).get('group')
        if not group_data:
            raise ValueError(f"Group not found or not accessible: {namespace}")
            
        print(f"[DEBUG] Total group members: {group_data['groupMembersCount']}")
        print(f"[DEBUG] Group URL: {group_data['webUrl']}")
        
        members_data = group_data['groupMembers']
        page_users = [member['user'] for member in members_data['nodes'] if member['user']]
        
        print(f"[DEBUG] Retrieved {len(page_users)} users from page {page}")
        all_users.extend(page_users)
        
        if not members_data['pageInfo']['hasNextPage']:
            break
            
        cursor = members_data['pageInfo']['endCursor']
        page += 1
    
    return all_users

def write_to_csv(users, namespace):
    """
    Write user data to CSV file.
    
    Args:
        users (list): List of user dictionaries
        namespace (str): The group path
    
    Returns:
        str: Name of the created file
    
    Raises:
        ValueError: If writing to CSV fails
    """
    timestamp = datetime.now().strftime('%Y_%m_%d_%H:%M')
    safe_namespace = namespace.replace('/', '_')
    filename = f"gitlab_user_by_activity_for_{safe_namespace}_{timestamp}.csv"
    
    print(f"[DEBUG] Writing data to {filename}...")
    
    headers = [
        'Username', 'Name', 'Public Email', 'Commit Email', 'User ID',
        'Last Activity Date', 'State', 'Is Bot', 'Is Human', 'Additional Emails'
    ]
    
    try:
        with open(filename, 'w', newline='', encoding='utf-8') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=headers)
            writer.writeheader()
            
            # Sort users by last activity date
            sorted_users = sorted(
                users,
                key=lambda x: x['lastActivityOn'] or '1970-01-01',
                reverse=True
            )
            
            for user in sorted_users:
                additional_emails = ';'.join(
                    [email['email'] for email in user.get('emails', {}).get('nodes', [])]
                ) if user.get('emails') else ''
                
                writer.writerow({
                    'Username': user['username'],
                    'Name': user['name'],
                    'Public Email': user['publicEmail'] or '',
                    'Commit Email': user['commitEmail'] or '',
                    'User ID': user['id'],
                    'Last Activity Date': user['lastActivityOn'] or '',
                    'State': user['state'],
                    'Is Bot': user['bot'],
                    'Is Human': user['human'],
                    'Additional Emails': additional_emails
                })
        return filename
    except IOError as e:
        raise ValueError(f"Failed to write CSV file: {str(e)}")

def main():
    """Main function to orchestrate the GitLab user activity data collection."""
    parser = argparse.ArgumentParser(
        description="Extract GitLab.com namespace user activity information"
    )
    parser.add_argument(
        "--namespace",
        required=True,
        help="GitLab namespace path (e.g., group/subgroup)"
    )
    parser.add_argument(
        "--token",
        required=True,
        help="GitLab.com Personal Access Token"
    )
    args = parser.parse_args()

    try:
        # Validate and normalize namespace
        namespace = validate_namespace_path(args.namespace)
        print(f"[DEBUG] Processing namespace: {namespace}")
        
        # Validate token
        validate_token(args.token)
        print("[DEBUG] Token validation successful")
        
        # Get all users with pagination
        users = get_all_users(args.token, namespace)
        
        # Write results to CSV
        output_file = write_to_csv(users, namespace)
        
        # Print summary
        print("\n[SUMMARY]")
        print(f"Total users processed: {len(users)}")
        print(f"Results have been saved to: {output_file}")
        
    except ValueError as e:
        print(f"Error: {str(e)}")
        sys.exit(1)
    except Exception as e:
        print(f"An unexpected error occurred: {str(e)}")
        sys.exit(1)

if __name__ == "__main__":
    main()
