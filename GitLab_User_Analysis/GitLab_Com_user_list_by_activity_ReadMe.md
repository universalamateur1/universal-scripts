# GitLab Group Member Activity Analyzer

A Python script that generates detailed user activity reports for GitLab.com groups. The script uses GitLab's GraphQL API to collect member information and creates a comprehensive CSV report sorted by last activity date.

## Features
- Collects detailed member information from GitLab groups
- Handles pagination for large groups automatically
- Includes multiple email addresses and activity data
- Sorts users by last activity date
- Supports bot detection and user state information

## Prerequisites
- Python 3.6 or higher
- Access to GitLab.com
- Personal Access Token with API access

## Quick Setup

1. Install Python dependencies:
```bash
pip install requests
```

2. Create GitLab Personal Access Token:
- Go to GitLab.com → User Settings → Access Tokens
- Create token with `api` scope
- Copy the generated token

3. (Optional) Set token as environment variable:
```bash
# Linux/macOS
export GITLAB_PRIVATE_TOKEN="your-token-here"

# Windows PowerShell
$env:GITLAB_PRIVATE_TOKEN="your-token-here"
```

## Usage

Basic usage:
```bash
python GitLab_Com_user_list_by_activity.py --namespace "group/subgroup" --token "your-token"
```

Using environment variable:
```bash
python GitLab_Com_user_list_by_activity.py --namespace "group/subgroup" --token $GITLAB_PRIVATE_TOKEN
```

### Arguments
- `--namespace`: Full path of the GitLab group (e.g., "gitlab-org/gitlab-foss")
- `--token`: GitLab Personal Access Token

## Output

Creates a CSV file with format:
`gitlab_user_by_activity_for_<group>_YYYY_MM_DD_HH:MM.csv`

### CSV Fields
- Username
- Name
- Public Email
- Commit Email
- User ID
- Last Activity Date
- State
- Is Bot
- Is Human
- Additional Emails

## Example
```bash
# For group https://gitlab.com/gitlab-org/gitlab-foss
python GitLab_Com_user_list_by_activity.py --namespace "gitlab-org/gitlab-foss" --token "glpat-xxxxxxxxxxxx"
```

## Debug Output
The script provides progress information:
- Connection status
- Group member count
- Pagination progress
- Data processing updates

## Error Handling
Includes validation for:
- Group accessibility
- Token validity
- API responses
- Data processing

## Support
For issues or questions contact the author @fsieversing

## Call:
`python3 ./GitLab_User_Analysis/GitLab_Com_user_list_by_activity.py --namespace "https://gitlab.com/gl-demo-ultimate-fsieverding" --token $GITLAB_PRIVATE_TOKEN`
`python3 ./GitLab_User_Analysis/GitLab_Com_user_list_by_activity.py --namespace "https://gitlab.com/groups/gl-demo-ultimate-fsieverding/" --token $GITLAB_PRIVATE_TOKEN`

`python3 ./GitLab_User_Analysis/GitLab_Com_user_list_by_activity.py --namespace "https://gitlab.com/gl-demo-ultimate-fsieverding" --token $GITLAB_PRIVATE_TOKEN --user-max 5`

`export GITLAB_PRIVATE_TOKEN="TOKEN"`


`python3 ./GitLab_User_Analysis/GitLab_Com_user_list_by_activity.py --namespace "https://gitlab.com/gitlab-com/" --token $GITLAB_PAT_TOKEN`

`export GITLAB_PAT_TOKEN="TOKEN"`

using [Query\.namespace](https://docs.gitlab.com/ee/api/graphql/reference/#querynamespace)

Create for me a short docuemtnaiton how to install the requirments for this script and call it as a readMe Markdown file.
