#!/usr/bin/env python3

"""
GitLab Group Member Activity Analyzer & Seat Manager

This script provides two main functionalities:
1. Activity Analysis: Retrieves and reports user activity data from GitLab groups
2. Seat Management: Optionally manages group size by removing inactive users

Features:
- Connects to GitLab.com via GraphQL API
- Generates detailed CSV reports of user activities
- Supports automatic seat management based on activity
- Handles pagination for large groups
- Provides audit trails for user removals

Usage:
    # Activity analysis only:
    python GitLab_Com_user_list_by_activity_and_removal.py --namespace "group/subgroup" --token "your-token"
    
    # Analysis and seat management:
    python GitLab_Com_user_list_by_activity_and_removal.py --namespace "group/subgroup" --token "your-token" --user-max 500
    
Environment variables:
    GITLAB_PRIVATE_TOKEN: Can be used to store the GitLab token

Output:
    - Activity report CSV: gitlab_user_by_activity_for_<group>_YYYY_MM_DD_HH:MM.csv
    - Removal report CSV: gitlab_removed_users_<group>_YYYY_MM_DD_HH:MM.csv (when using --user-max)

Author: Falko Sieverding
Version: 1.1.0
"""

import argparse
import csv
import json
import re
import requests
import sys
from datetime import datetime
from urllib.parse import urlparse, quote

def validate_namespace_path(namespace):
    """
    Validate and normalize the GitLab namespace path.
    
    Processes various input formats (URL, path) and normalizes them to a standard format.
    Supports inputs like:
    - https://gitlab.com/group/subgroup
    - gitlab.com/group/subgroup
    - group/subgroup
    
    Args:
        namespace (str): The input namespace to validate. Can be a full URL or path.
    
    Returns:
        str: The normalized namespace path (e.g., "group/subgroup")
    
    Raises:
        ValueError: If the namespace format is invalid or cannot be parsed
    
    Example:
        >>> validate_namespace_path("https://gitlab.com/group/subgroup")
        "group/subgroup"
    """
    # Remove any GitLab.com URL parts if provided
    namespace = namespace.replace('https://gitlab.com/', '')
    namespace = namespace.replace('gitlab.com/', '')
    namespace = namespace.strip('/')
    
    # Validate namespace format
    if not re.match(r'^[a-zA-Z0-9_/-]+$', namespace):
        raise ValueError("Invalid namespace format. Use format: group/subgroup")
    
    return namespace

def validate_token(token):
    """
    Validate if the provided token has sufficient permissions.
    
    Tests the token against GitLab's API to ensure it has the required permissions
    for both reading group data and managing members if needed.
    
    Args:
        token (str): The GitLab API token to validate
    
    Returns:
        bool: True if token is valid and has sufficient permissions
    
    Raises:
        ValueError: If token is invalid, expired, or lacks required permissions
    
    Note:
        Requires at least API read access, and API write access for --user-max functionality
    """
    print("[DEBUG] Validating token permissions...")
    
    headers = {'Authorization': f'Bearer {token}'}
    
    try:
        response = requests.get(
            "https://gitlab.com/api/v4/user",
            headers=headers,
            timeout=10
        )
        if response.status_code != 200:
            raise ValueError("Invalid token or insufficient permissions")
        return True
    except requests.exceptions.RequestException as e:
        raise ValueError(f"Failed to validate token: {str(e)}")

def execute_graphql_query(token, namespace, cursor=None):
    """
    Execute the GraphQL query to get group member data with pagination support.
    
    Retrieves detailed user information including:
    - Basic user data (username, name, emails)
    - Activity information
    - Bot status
    - Email addresses (public, commit, additional)
    
    Args:
        token (str): The GitLab API token
        namespace (str): The group path
        cursor (str, optional): Pagination cursor for subsequent pages
    
    Returns:
        dict: Query results containing user data and pagination information
    
    Raises:
        ValueError: If the query fails, returns errors, or access is denied
    
    Note:
        Uses cursor-based pagination with a page size of 100 users
    """
    print(f"[DEBUG] Executing GraphQL query{' with cursor: ' + cursor if cursor else ''}...")
    
    headers = {
        'Authorization': f'Bearer {token}',
        'Content-Type': 'application/json',
    }
    
    query = """
    query GetGroupMembers($fullPath: ID!, $after: String) {
      group(fullPath: $fullPath) {
        groupMembersCount
        fullPath
        webUrl
        groupMembers(
          relations: [DIRECT],
          first: 100,
          after: $after
        ) {
          pageInfo {
            endCursor
            hasNextPage
          }
          nodes {
            user {
              id
              username
              name
              publicEmail
              lastActivityOn
              state
              bot
              human
              commitEmail
              emails {
                nodes {
                  email
                }
              }
            }
          }
        }
      }
    }
    """
    
    variables = {
        "fullPath": namespace,
        "after": cursor
    }
    
    try:
        response = requests.post(
            "https://gitlab.com/api/graphql",
            headers=headers,
            json={'query': query, 'variables': variables},
            timeout=30
        )
        
        if response.status_code != 200:
            print(f"[ERROR] Query failed with status code: {response.status_code}")
            print(f"[ERROR] Response: {response.text}")
            raise ValueError(f"GraphQL query failed with status code: {response.status_code}")
        
        result = response.json()
        
        if 'errors' in result:
            print("[ERROR] GraphQL Query Errors:")
            for error in result['errors']:
                print(f"  {error.get('message')}")
            raise ValueError("GraphQL query returned errors")
            
        return result
        
    except requests.exceptions.RequestException as e:
        raise ValueError(f"Failed to execute GraphQL query: {str(e)}")

def get_all_users(token, namespace):
    """
    Retrieve all users from the group using pagination.
    
    Args:
        token (str): The GitLab API token
        namespace (str): The group path
    
    Returns:
        list: List of user data dictionaries
    """
    print(f"[DEBUG] Retrieving all users for group: {namespace}")
    
    all_users = []
    cursor = None
    page = 1
    
    while True:
        result = execute_graphql_query(token, namespace, cursor)
        
        group_data = result.get('data', {}).get('group')
        if not group_data:
            raise ValueError(f"Group not found or not accessible: {namespace}")
            
        print(f"[DEBUG] Total group members: {group_data['groupMembersCount']}")
        print(f"[DEBUG] Group URL: {group_data['webUrl']}")
        
        members_data = group_data['groupMembers']
        page_users = [member['user'] for member in members_data['nodes'] if member['user']]
        
        print(f"[DEBUG] Retrieved {len(page_users)} users from page {page}")
        all_users.extend(page_users)
        
        if not members_data['pageInfo']['hasNextPage']:
            break
            
        cursor = members_data['pageInfo']['endCursor']
        page += 1
    
    return all_users

def write_to_csv(users, namespace):
    """
    Write user data to CSV file.
    
    Args:
        users (list): List of user dictionaries
        namespace (str): The group path
    
    Returns:
        str: Name of the created file
    
    Raises:
        ValueError: If writing to CSV fails
    """
    timestamp = datetime.now().strftime('%Y_%m_%d_%H:%M')
    safe_namespace = namespace.replace('/', '_')
    filename = f"gitlab_user_by_activity_for_{safe_namespace}_{timestamp}.csv"
    
    print(f"[DEBUG] Writing data to {filename}...")
    
    headers = [
        'Username', 'Name', 'Public Email', 'Commit Email', 'User ID',
        'Last Activity Date', 'State', 'Is Bot', 'Is Human', 'Additional Emails'
    ]
    
    try:
        with open(filename, 'w', newline='', encoding='utf-8') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=headers)
            writer.writeheader()
            
            # Sort users by last activity date
            sorted_users = sorted(
                users,
                key=lambda x: x['lastActivityOn'] or '1970-01-01',
                reverse=True
            )
            
            for user in sorted_users:
                additional_emails = ';'.join(
                    [email['email'] for email in user.get('emails', {}).get('nodes', [])]
                ) if user.get('emails') else ''
                
                writer.writerow({
                    'Username': user['username'],
                    'Name': user['name'],
                    'Public Email': user['publicEmail'] or '',
                    'Commit Email': user['commitEmail'] or '',
                    'User ID': user['id'],
                    'Last Activity Date': user['lastActivityOn'] or '',
                    'State': user['state'],
                    'Is Bot': user['bot'],
                    'Is Human': user['human'],
                    'Additional Emails': additional_emails
                })
        return filename
    except IOError as e:
        raise ValueError(f"Failed to write CSV file: {str(e)}")

def remove_group_member(token, group_id, user_id):
    """
    Remove a billable member from a group using GitLab REST API.
    
    Implements the GitLab API endpoint:
    DELETE /api/v4/groups/:id/members/:user_id
    
    Args:
        token (str): The GitLab API token
        group_id (str): The group ID or path
        user_id (int): The user ID to remove
    
    Returns:
        bool: True if removal was successful, False otherwise
    
    Raises:
        ValueError: If the API call fails or returns an error
    
    Note:
        This operation is irreversible and immediately removes the user's access
    """
    print(f"[DEBUG] Attempting to remove user {user_id} from group {group_id}...")
    
    headers = {'Authorization': f'Bearer {token}'}
    url = f"https://gitlab.com/api/v4/groups/{group_id}/members/{user_id}"
    
    try:
        response = requests.delete(url, headers=headers, timeout=10)
        
        if response.status_code == 204:
            print(f"[DEBUG] Successfully removed user {user_id}")
            return True
        else:
            print(f"[ERROR] Failed to remove user {user_id}. Status code: {response.status_code}")
            print(f"[ERROR] Response: {response.text}")
            return False
            
    except requests.exceptions.RequestException as e:
        print(f"[ERROR] API call failed for user {user_id}: {str(e)}")
        return False

def write_removed_users_csv(users, namespace, timestamp):
    """
    Write removed user data to a separate CSV file for auditing.
    
    Creates a detailed record of removed users including:
    - User identification (ID, username, name)
    - Contact information (emails)
    - Activity data
    - Removal status and timestamp
    
    Args:
        users (list): List of removed user dictionaries
        namespace (str): The group path
        timestamp (str): Timestamp for filename
    
    Returns:
        str: Name of the created CSV file
    
    Note:
        This file serves as an audit trail for user removals
    """
    safe_namespace = namespace.replace('/', '_')
    filename = f"gitlab_removed_users_{safe_namespace}_{timestamp}.csv"
    
    print(f"[DEBUG] Writing removed users data to {filename}...")
    
    headers = [
        'Username', 'Name', 'Public Email', 'Commit Email', 'User ID',
        'Last Activity Date', 'State', 'Is Bot', 'Is Human', 'Additional Emails',
        'Removal Status', 'Removal Time'
    ]
    
    try:
        with open(filename, 'w', newline='', encoding='utf-8') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=headers)
            writer.writeheader()
            
            for user in users:
                additional_emails = ';'.join(
                    [email['email'] for email in user.get('emails', {}).get('nodes', [])]
                ) if user.get('emails') else ''
                
                writer.writerow({
                    'Username': user['username'],
                    'Name': user['name'],
                    'Public Email': user['publicEmail'] or '',
                    'Commit Email': user['commitEmail'] or '',
                    'User ID': user['id'],
                    'Last Activity Date': user['lastActivityOn'] or '',
                    'State': user['state'],
                    'Is Bot': user['bot'],
                    'Is Human': user['human'],
                    'Additional Emails': additional_emails,
                    'Removal Status': user.get('removal_status', 'Unknown'),
                    'Removal Time': user.get('removal_time', '')
                })
        return filename
    except IOError as e:
        raise ValueError(f"Failed to write removed users CSV file: {str(e)}")

def process_user_removal(token, namespace, users, user_max):
    """
    Process user removal based on maximum user limit.
    
    Implements the seat management logic:
    1. Identifies users exceeding the limit
    2. Sorts users by last activity (least active first)
    3. Attempts to remove excess users
    4. Tracks successful and failed removals
    
    Args:
        token (str): The GitLab API token
        namespace (str): The group path
        users (list): List of all users
        user_max (int): Maximum number of users allowed
    
    Returns:
        tuple: (removed_users, remaining_users)
            - removed_users: List of successfully removed users
            - remaining_users: List of users still in the group
    
    Note:
        Users are removed based on least recent activity
    """
    total_users = len(users)
    users_to_remove = total_users - user_max
    
    if users_to_remove <= 0:
        print(f"[INFO] No users need to be removed. Current users ({total_users}) <= max users ({user_max})")
        return [], users
    
    print(f"[DEBUG] Need to remove {users_to_remove} users to meet limit of {user_max}")
    
    # Sort users by last activity, oldest first
    sorted_users = sorted(
        users,
        key=lambda x: x['lastActivityOn'] or '1970-01-01'
    )
    
    users_to_deactivate = sorted_users[:users_to_remove]
    remaining_users = sorted_users[users_to_remove:]
    
    removed_users = []
    failed_removals = []
    
    print(f"[DEBUG] Starting removal process for {len(users_to_deactivate)} users...")
    
    for user in users_to_deactivate:
        user_id = user['id'].split('/')[-1]  # Extract numeric ID from global ID
        removal_time = datetime.now().isoformat()
        
        if remove_group_member(token, namespace, user_id):
            user['removal_status'] = 'Success'
            user['removal_time'] = removal_time
            removed_users.append(user)
        else:
            user['removal_status'] = 'Failed'
            user['removal_time'] = removal_time
            failed_removals.append(user)
    
    print(f"\n[REMOVAL SUMMARY]")
    print(f"Successfully removed: {len(removed_users)} users")
    print(f"Failed to remove: {len(failed_removals)} users")
    
    return removed_users, remaining_users

def main():
    """
    Main function to orchestrate the GitLab user management process.
    
    Workflow:
    1. Parse and validate command-line arguments
    2. Validate GitLab access (URL and token)
    3. Retrieve user data with pagination
    4. Generate activity report
    5. Optionally process user removals if --user-max is specified
    6. Generate removal report if users were removed
    7. Display summary statistics
    
    Command-line Arguments:
        --namespace: GitLab group path (required)
        --token: GitLab API token (required)
        --user-max: Maximum allowed users (optional)
    
    Exit Codes:
        0: Success
        1: Error (invalid input, API error, etc.)
    """
    parser = argparse.ArgumentParser(
        description="Extract and manage GitLab.com namespace user information"
    )
    parser.add_argument(
        "--namespace",
        required=True,
        help="GitLab namespace path (e.g., group/subgroup)"
    )
    parser.add_argument(
        "--token",
        required=True,
        help="GitLab.com Personal Access Token"
    )
    parser.add_argument(
        "--user-max",
        type=int,
        help="Maximum number of users to maintain in the namespace"
    )
    args = parser.parse_args()

    try:
        # Validate and normalize namespace
        namespace = validate_namespace_path(args.namespace)
        print(f"[DEBUG] Processing namespace: {namespace}")
        
        # Validate token
        validate_token(args.token)
        print("[DEBUG] Token validation successful")
        
        # Get all users with pagination
        users = get_all_users(args.token, namespace)
        
        # Generate timestamp for filenames
        timestamp = datetime.now().strftime('%Y_%m_%d_%H:%M')
        
        # Write initial user list to CSV
        output_file = write_to_csv(users, namespace)
        print(f"[DEBUG] Initial user list saved to: {output_file}")
        
        # Process user removal if max users specified
        if args.user_max:
            removed_users, remaining_users = process_user_removal(
                args.token,
                namespace,
                users,
                args.user_max
            )
            
            if removed_users:
                removed_file = write_removed_users_csv(removed_users, namespace, timestamp)
                print(f"[DEBUG] Removed users list saved to: {removed_file}")
        
            print("\n[FINAL SUMMARY]")
            print(f"Initial user count: {len(users)}")
            print(f"Users removed: {len(removed_users)}")
            print(f"Remaining users: {len(remaining_users)}")
        else:
            print("\n[SUMMARY]")
            print(f"Total users processed: {len(users)}")
            print(f"Results have been saved to: {output_file}")
        
    except ValueError as e:
        print(f"Error: {str(e)}")
        sys.exit(1)
    except Exception as e:
        print(f"An unexpected error occurred: {str(e)}")
        sys.exit(1)

if __name__ == "__main__":
    main()
