#!/usr/bin/env python3
import argparse
import re
import requests
import gitlab

def check_gitlab_instance(url):
    """
    Check if the provided URL is a reachable and valid GitLab instance.

    Args: url (str): The URL of the GitLab instance to check.

    Returns: str: The validated URL of the GitLab instance.

    Raises: SystemExit: If the provided URL is invalid, unreachable, or not a GitLab instance.
    """

    try:
        # Ensure Url ends with /
        if not url.endswith('/'):
            url += '/'
        # Ensure the URL starts with http:// or https://
        if not re.match(r'^https?://', url):
            raise ValueError(f"Invalid URL format. Please provide a URL starting with http:// or https://")
        # Ensure the URL is reachable
        response = requests.get(url)
        response.raise_for_status()
        # Check for GitLab-specific headers or content
        if 'GitLab' not in response.text:
            raise ValueError(f"The provided URL does not seem to be a GitLab instance.")
    # Raise for errors
    except (requests.exceptions.RequestException, ValueError) as e:
        raise SystemExit(f"Error: Unable to reach or validate the GitLab instance at {url}. Please check the URL and try again. {e}")
    return url

def check_admin_token(url, headers):
    """
    Check if the provided access token has admin permissions on the GitLab instance.

    Args: url (str): The URL of the GitLab instance. 
    headers (dict): The headers containing the access token for authentication.

    Returns: dict: The user data of the authenticated admin user.

    Raises: SystemExit: If the provided token is invalid, unable to authenticate, or lacks admin permissions.
    """
    try:
        response = requests.get(f"{url}/api/v4/user", headers=headers)
        response.raise_for_status()
        user_data = response.json()
        if not user_data.get('is_admin', False):
            raise ValueError("The provided token does not have admin permissions.")
    except (requests.exceptions.RequestException, ValueError) as e:
        raise SystemExit(f"Error: Invalid token, unable to authenticate, or insufficient permissions. {e}")
    return user_data

def main(gitlab_url, access_token):
    """
    The main function to validate the GitLab instance and admin token, after it initializes the Connection for gitlab-python.
    We then call a list of all users and append those into sorted list for later manipulation

    Args: 
        gitlab_url (str): The URL of the GitLab instance.
        access_token (str): The GitLab access token with admin permissions.
    """
    # Check if the GitLab instance is reachable and valid otherwise raise for errors
    checked_gitlab_url = check_gitlab_instance(gitlab_url)
    # Define headers for authentication
    headers = {'Authorization': f'Bearer {access_token}'}
    # Check if the token is valid and has admin permissions and give in Success case a text message to the Console
    user_data = check_admin_token(checked_gitlab_url, headers)
    print(f"Successfully authenticated as user: {user_data['username']} with admin permissions.")

    # Initialize GitLab client
    gl = gitlab.Gitlab(url=checked_gitlab_url, private_token=access_token)

    # Lists to store categorized users
    admin_users = []
    auditor_users = []
    no_memberships = []
    non_billable_users = []
    inactive_users = []
    active_users = []
    # Prep Variables for Pagination call with API
    page = 1
    per_page = 100  # Adjust the number of users per page as needed
    all_users = []
    # Get the list of users matching the criteria
    print(f'Calling the GitLab Api of {checked_gitlab_url} and retrieving all active, non-blocked, none bot users...')
    while True:
        print(f"Retrieving users for page {page}...")
        users = gl.users.list(active=True, bot=False, blocked=False, page=page, per_page=per_page)
        if not users:
            break  # No more users, exit the loop
        all_users.extend(users)
        page += 1

    # Print the user details
    for user in all_users:
        print(f"User ID: {user.id}, Username: {user.username}, Email: {user.email}, is admin: {user.is_admin}")
        if user.is_admin:
            admin_users.append(user)
        elif user.is_auditor:
            auditor_users.append(user)
        else:
            active_users.append(user)
    print(f'Number of Admin Users: {len(admin_users)}')
    print(f'Number of Auditor Users: {len(auditor_users)}')
    print(f'Number of Active None Admin Users: {len(active_users)}')


                                     
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Validate GitLab instance and admin token.')
    parser.add_argument('--instance-url', type=str, required=True, help='The URL of the GitLab instance')
    parser.add_argument('--admin-token', type=str, required=True, help='The GitLab access token with admin permissions')

    args = parser.parse_args()
    main(args.instance_url, args.admin_token)