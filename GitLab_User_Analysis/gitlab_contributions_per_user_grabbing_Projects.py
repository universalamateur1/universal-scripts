
#!/usr/bin/env python3
import sys
import argparse
from typing import List, Dict, Any
import gitlab
import requests
from urllib.parse import urlparse

def parse_arguments():
    """
    Parse command-line arguments.
    
    Returns:
        argparse.Namespace: Parsed arguments
    """
    parser = argparse.ArgumentParser(description="Fetch GitLab user contributions")
    parser.add_argument("--url", default="https://gitlab.com", help="GitLab server URL (default: https://gitlab.com)")
    parser.add_argument("--token", help="Personal Access Token (optional for gitlab.com)")
    parser.add_argument("--users", nargs="+", required=True, help="List of GitLab usernames")
    return parser.parse_args()

def validate_gitlab_connection(url, token):
    """
    Validate the GitLab server connection and authentication.
    
    Args:
        url (str): GitLab server URL
        token (str): Personal Access Token (optional for gitlab.com)
    
    Returns:
        gitlab.Gitlab: Authenticated GitLab client object
    """
    # Check if the URL is valid
    parsed_url = urlparse(url)
    if not parsed_url.scheme or not parsed_url.netloc:
        print(f"Error: Invalid URL format: {url}")
        sys.exit(1)

    # If not gitlab.com, check if the server is reachable
    if url != "https://gitlab.com":
        try:
            response = requests.get(f"{url}/api/v4/version", timeout=5)
            if response.status_code != 200:
                print(f"Error: Unable to reach GitLab server at {url}. Status code: {response.status_code}")
                sys.exit(1)
        except requests.exceptions.RequestException as e:
            print(f"Error: Unable to connect to GitLab server at {url}. {str(e)}")
            sys.exit(1)

    # Create GitLab client object
    if token:
        gl = gitlab.Gitlab(url, private_token=token)
        try:
            gl.auth()
            print(f"Successfully connected to GitLab server: {url} (authenticated)")
        except Exception as e:
            print(f"Error: Unable to authenticate with GitLab server {url}. {str(e)}")
            sys.exit(1)
    else:
        if url != "https://gitlab.com":
            print("Error: Token is required for non-gitlab.com servers")
            sys.exit(1)
        gl = gitlab.Gitlab(url)
        print(f"Connected to GitLab server: {url} (unauthenticated)")
    
    return gl

def validate_users(gl, usernames):
    """
    Validate the existence of provided usernames on the GitLab server.
    
    Args:
        gl (gitlab.Gitlab): GitLab client object
        usernames (List[str]): List of usernames to validate
    
    Returns:
        List[gitlab.v4.objects.User]: List of valid GitLab user objects
    """
    valid_users = []
    for username in usernames:
        try:
            user = gl.users.list(username=username)[0]
            valid_users.append(user)
            print(f"User '{username}' found.")
        except IndexError:
            print(f"Warning: User '{username}' not found. Skipping.")
        except Exception as e:
            print(f"Error: Unable to validate user '{username}'. {str(e)}")
    return valid_users

def get_user_contributions(gl, user):
    """
    Fetch contributions for a given user across accessible projects.
    
    Args:
        gl (gitlab.Gitlab): GitLab client object
        user (gitlab.v4.objects.User): GitLab user object
    
    Returns:
        Dict[str, Any]: Dictionary of user contributions by project
    """
    contributions = {}
    
    # Fetch projects the user has access to
    try:
        projects = gl.projects.list(as_list=False, all=True)
    except gitlab.exceptions.GitlabAuthenticationError:
        print(f"Warning: Unable to fetch projects. Unauthenticated access has limited capabilities.")
        return contributions
    # Debug print after call finished
    print("Finished fetching projects.")
    print(f"Found {projects.total} projects accessible to the user {user.name}.")

    for project in projects:
        # Debug Print of ProjectName
        print(f'Fetching details for project: {project.name}')
        project_contributions = {
            "merge_requests": [],
            "issues": [],
            "other_activities": []
        }
        
        try:
            # Fetch Merge Requests
            mrs = project.mergerequests.list(author_id=user.id, all=True)
            #Debug Print of MR
            print(f'Found {len(mrs)} merge requests for project {project.name} by user {user.name}.')
            for mr in mrs:
                project_contributions["merge_requests"].append({
                    "iid": mr.iid,
                    "title": mr.title,
                    "state": mr.state,
                    "created_at": mr.created_at,
                    "closed_at": mr.closed_at,
                    "commit_count": len(mr.commits()) if mr.state == "merged" else None
                })
            
            # Fetch Issues
            issues = project.issues.list(author_id=user.id, all=True)
            #Debug Print of Issues
            print(f'Found {len(issues)} issues for project {project.name} by user {user.name}.')
            for issue in issues:
                project_contributions["issues"].append({
                    "iid": issue.iid,
                    "title": issue.title,
                    "state": issue.state,
                    "created_at": issue.created_at,
                    "closed_at": issue.closed_at
                })
            
            # Fetch other activities (e.g., comments)
            events = project.events.list(action_type="commented", all=True)
            #Debug Print of Events
            print(f'Found {len(events)} events for project {project.name} by user {user.name}.')
            for event in events:
                if event.author_id == user.id:
                    project_contributions["other_activities"].append({
                        "action_name": event.action_name,
                        "target_type": event.target_type,
                        "created_at": event.created_at
                    })
        
        except gitlab.exceptions.GitlabAuthenticationError:
            print(f"Warning: Unable to fetch details for project {project.name}. Skipping.")
            continue
        
        if any(project_contributions.values()):
            contributions[project.name] = project_contributions
    
    return contributions


def display_contributions(username, contributions):
    """
    Display user contributions in a formatted manner.
    
    Args:
        username (str): Username of the GitLab user
        contributions (Dict[str, Any]): Dictionary of user contributions by project
    """
    print(f"\nContributions for user: {username}")
    for project_name, project_data in contributions.items():
        print(f"\nProject: {project_name}")
        
        print("  Merge Requests:")
        for mr in project_data["merge_requests"]:
            print(f"    - #{mr['iid']} {mr['title']} (Status: {mr['state']})")
            print(f"      Opened: {mr['created_at']}, Closed: {mr['closed_at'] or 'N/A'}")
            if mr['commit_count'] is not None:
                print(f"      Commits: {mr['commit_count']}")
        
        print("  Issues:")
        for issue in project_data["issues"]:
            print(f"    - #{issue['iid']} {issue['title']} (Status: {issue['state']})")
            print(f"      Opened: {issue['created_at']}, Closed: {issue['closed_at'] or 'N/A'}")
        
        print("  Other Activities:")
        for activity in project_data["other_activities"]:
            print(f"    - {activity['action_name']} on {activity['target_type']} at {activity['created_at']}")

def main():
    """
    Main function to orchestrate the GitLab user contributions retrieval process.
    """
    args = parse_arguments()
    gl = validate_gitlab_connection(args.url, args.token)
    valid_users = validate_users(gl, args.users)
    # Debug Print
    print(f'These are the Valid user: {valid_users}')
    for user in valid_users:
        # Debug Print
        print(f'Starting to fetch contributions for user: {user.username}')
        contributions = get_user_contributions(gl, user)
        if contributions:
            display_contributions(user.username, contributions)
        else:
            print(f"\nNo contributions found for user: {user.username}")
            print("This could be due to limited access or the user having no public contributions.")    

if __name__ == "__main__":
    main()
