# Docs

With gitlab-python and an established gitlab object the call 'gl.users.list(active=True, blocked=False, bot=False)' gives back 20 user objects.

The user object returns thses attributes of interest:

- id: The unique ID of the user (integer)
- username: The username of the user (string)
- email: The email address of the user (string)
- name: The full name of the user (string)
- state: The state of the user (string, e.g., 'active', 'blocked')
- created_at: The timestamp when the user was created (datetime)
- last_sign_in_at: The timestamp of the user's last sign-in (datetime)
- last_activity_on: The timestamp of the user's last activity (datetime)
- external: Whether the user is an external user (boolean)
- is_admin: Whether the user is an admin (boolean)
