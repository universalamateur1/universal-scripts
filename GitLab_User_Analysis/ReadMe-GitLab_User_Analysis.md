# Identify User behaviour in GitLab

## Purpose of these Scripts

Identify the User in a GitLab Instance, Namespace, Group and/or project and list the highest necessary Permission based on their past behaviour they would need in this GitLab Instance, Namespace, Group and/or project

## Scope

Goal for the first iteration is to analyse and recommend changes based on the default project permission roles Guest, Reporter, Developer, Maintainer and Owner.
We want to analyze past behaviour of the user and see if they could live with a Guest User role or would need a Reporter role or a hogher role.

### Definiton Core Development user

core development tasks:

- pushing Code
- creating merge requests
- approving merge requests
- destroying environments
- trigger pipelines
- manage configuration
- manage members

## Functionality

### Boilerplate in BoilerPlate_Check-GitLab-Access.py

1. Get a Url and a token over the Command line call.
2. Check if the Url is Valid and a GitLab instance.
3. Validate the Token.
4. Validate the token has admin permissions.

## Local testing - Using the GitLab Scripts

### Venv

```bash
python3 -m venv ~/git/universal-scripts/.
source ~/git/universal-scripts/./bin/activate
```

- Confirm `which python`

### Pip

```bash
python3 -m pip install --upgrade pip
python3 -m pip --version
```

### Reqs

```bash
`python3 -m pip freeze >| ./GitLab_User_Analysis/requirements.txt`
```

```bash
python3 -m pip install -r ./GitLab_User_Analysis/requirements.txt
```

## Projects and resources

- Prompt [https://gitlab.com/universalamateur1/ua-privatre/personal-zettelkasten/-/blob/master/MyAiPrompts/Programming_prompt.md]
- Demo Env [https://cs.gitlabdemo.cloud/admin/users]
- Demo User [https://cs.gitlabdemo.cloud/admin/users/universalamateur/]
- [https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/remove-inactive-group-members]
- [https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/make-guest-users]
- [https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/block-inactive-users]
- [https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/report-gitLab-user-activity-since-date]
- [https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/potential-guest-users]
- 
