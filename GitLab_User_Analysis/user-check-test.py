#!/usr/bin/env python3
import argparse
import re
import requests
import gitlab
from datetime import datetime, timedelta

# Boiler Plate must be changed

def check_gitlab_instance(url):
    try:
        # Ensure the URL starts with http:// or https://
        if not re.match(r'^https?://', url):
            raise ValueError(f"Invalid URL format. Please provide a URL starting with http:// or https://")
        
        response = requests.get(url)
        response.raise_for_status()
        
        # Check for GitLab-specific headers or content
        if 'GitLab' not in response.text:
            raise ValueError(f"The provided URL does not seem to be a GitLab instance.")
        
    except (requests.exceptions.RequestException, ValueError) as e:
        raise SystemExit(f"Error: Unable to reach or validate the GitLab instance at {url}. Please check the URL and try again. {e}")

def check_admin_token(url, headers):
    try:
        response = requests.get(f"{url}/api/v4/user", headers=headers)
        response.raise_for_status()
        user_data = response.json()
        if not user_data.get('is_admin', False):
            raise ValueError("The provided token does not have admin permissions.")
    except (requests.exceptions.RequestException, ValueError) as e:
        raise SystemExit(f"Error: Invalid token, unable to authenticate, or insufficient permissions. {e}")
    return user_data

def main(gitlab_url, access_token):
    # Check if the GitLab instance is reachable and valid otherwise raise for errors
    check_gitlab_instance(gitlab_url)

    # Define headers for authentication
    headers = {'Authorization': f'Bearer {access_token}'}

    # Check if the token is valid and has admin permissions and give in Success case a text message to the Console
    user_data = check_admin_token(gitlab_url, headers)
    print(f"Successfully authenticated as user: {user_data['username']} with admin permissions.")

def has_memberships(gl, user):
    """Check if a user has any memberships in projects or groups."""
    memberships = user.memberships.list(iterator=True)
    if memberships:
        return True
    return False

def highest_role(gl, user):
    """Determine the highest role a user has across their memberships."""
    roles = set()
    for project in user.projects.list():
        roles.add(project.access_level)
    for group in user.groups.list():
        roles.add(group.access_level)
    return max(roles) if roles else None

def main(gitlab_url, access_token, inactive_days=30):
    # Check if the GitLab instance is reachable and valid
    check_gitlab_instance(gitlab_url)

    # Define headers for authentication
    headers = {'Authorization': f'Bearer {access_token}'}

    # Check if the token is valid and has admin permissions
    user_data = check_admin_token(gitlab_url, headers)
    print(f"Successfully authenticated as user: {user_data['username']} with admin permissions.")

    # Initialize GitLab client
    gl = gitlab.Gitlab(url=gitlab_url, private_token=access_token)

    # Lists to store categorized users
    admin_users = []
    auditor_users = []
    no_memberships = []
    non_billable_users = []
    inactive_users = []
    active_users = []

    # Get all users
    users = gl.users.list(all=True)

    # Filter out bot accounts
    bot_patterns = [r'^bot', r'bot$', r'_bot_', r'-bot-']
    users = [user for user in users if not any(re.search(pattern, user.username, re.IGNORECASE) for pattern in bot_patterns)]

    inactive_threshold = datetime.now() - timedelta(days=inactive_days)

    for user in users:
        # Check if user is admin
        if user.is_admin:
            admin_users.append(user)
            continue

        # Check if user is auditor
        if "is_auditor" in user.attributes:
                if user.attributes["is_auditor"]:
                    auditor_users.append(user)
                    continue

        # Check if user has no memberships
        if not has_memberships(gl, user):
            no_memberships.append(user)
            continue

        # Check if user is not using a license seat
        if not user.using_license_seat:
            non_billable_users.append(user)
            continue

        # Check if user is inactive
        last_activity = max(
            datetime.strptime(user.current_sign_in_at, "%Y-%m-%dT%H:%M:%SZ"),
            datetime.strptime(user.last_activity_on, "%Y-%m-%dT%H:%M:%SZ"),
            datetime.strptime(user.created_at, "%Y-%m-%dT%H:%M:%SZ")
        ) if all([user.current_sign_in_at, user.last_activity_on, user.created_at]) else None

        if last_activity and last_activity < inactive_threshold:
            inactive_users.append(user)
        else:
            role = highest_role(gl, user)
            active_users.append((user, role))

    # Print summary statistics
    print("\nSummary Statistics:")
    print(f"Total Users: {len(users)}")
    print(f"Active Users: {len(active_users)}")
    print(f"Inactive Users: {len(inactive_users)}")
    print(f"Admin Users: {len(admin_users)}")
    print(f"Auditor Users: {len(auditor_users)}")
    print(f"Users with No Memberships: {len(no_memberships)}")
    print(f"Non-Billable Users: {len(non_billable_users)}")
                                     
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Validate GitLab instance and admin token.')
    parser.add_argument('--instance-url', type=str, required=True, help='The URL of the GitLab instance')
    parser.add_argument('--admin-token', type=str, required=True, help='The GitLab access token with admin permissions')

    args = parser.parse_args()
    main(args.instance_url, args.admin_token)