# GitLab Group Member Activity Analyzer & Seat Manager

A Python utility for analyzing and managing GitLab group member activities. This tool helps track user activity, generate detailed reports, and optionally manage group membership based on activity levels.

## Features

- Retrieves detailed member information from GitLab groups
- Generates activity reports sorted by last activity date
- Supports automatic user management based on seat limits
- Handles pagination for large groups
- Provides comprehensive user data including multiple email addresses
- Supports both reporting and management operations

## Prerequisites

- Python 3.6 or higher
- Access to GitLab.com
- Personal Access Token with API scope
- `requests` Python package

## Installation

1. Clone or download the script:
```bash
curl -O https://gitlab.com/-/snippets/4765241/raw/main/GitLab_Com_user_list_by_activity.py
```

2. Create a virtual environment (recommended):
```bash
# Linux/macOS
python3 -m venv venv
source venv/bin/activate

# Windows
python -m venv venv
.\venv\Scripts\activate
```

3. Install required package:
```bash
pip install requests
```

4. Set up GitLab access:
   - Go to GitLab.com → Settings → Access Tokens
   - Create a new token with `api` scope
   - Save the token securely

## Configuration

Set your GitLab token as an environment variable (optional):
```bash
# Linux/macOS
export GITLAB_PRIVATE_TOKEN="your-token-here"

# Windows PowerShell
$env:GITLAB_PRIVATE_TOKEN="your-token-here"
```

## Usage

### Basic Activity Report
Generate a report of all users sorted by activity:
```bash
python GitLab_Com_user_list_by_activity.py \
  --namespace "group/subgroup" \
  --token "your-token"
```

### Manage User Limits
Generate report and remove least active users exceeding the limit:
```bash
python GitLab_Com_user_list_by_activity.py \
  --namespace "group/subgroup" \
  --token "your-token" \
  --user-max 500
```

## Command Line Options

- `--namespace`: Full path of the GitLab group (required)
  - Example: "gitlab-org/gitlab-foss"
  - Can be found in the group's URL path

- `--token`: GitLab Personal Access Token (required)
  - Must have API scope
  - Can be set via environment variable

- `--user-max`: Maximum number of users to maintain (optional)
  - Integer value
  - Will remove least active users exceeding this limit

## Output Files

### Activity Report
- Filename: `gitlab_user_by_activity_for_<group>_YYYY_MM_DD_HH:MM.csv`
- Contains:
  - Username
  - Name
  - Public Email
  - Commit Email
  - User ID
  - Last Activity Date
  - State
  - Bot Status
  - Additional Emails

### Removed Users Report (when using --user-max)
- Filename: `gitlab_removed_users_<group>_YYYY_MM_DD_HH:MM.csv`
- Additional fields:
  - Removal Status
  - Removal Time

## Examples

### Generate Activity Report
```bash
# Basic usage
python GitLab_Com_user_list_by_activity.py \
  --namespace "my-org/my-group" \
  --token "glpat-XXXXXXXXXXXXX"

# Using environment variable for token
python GitLab_Com_user_list_by_activity.py \
  --namespace "my-org/my-group" \
  --token $GITLAB_PRIVATE_TOKEN
```

### Manage Group Size
```bash
# Limit group to 500 users
python GitLab_Com_user_list_by_activity.py \
  --namespace "my-org/my-group" \
  --token "glpat-XXXXXXXXXXXXX" \
  --user-max 500

# Analyze large group with environment token
python GitLab_Com_user_list_by_activity.py \
  --namespace "my-org/my-group" \
  --token $GITLAB_PRIVATE_TOKEN \
  --user-max 1000
```

## Debug Output

The script provides detailed progress information:
- Connection status
- Token validation
- Member count
- Pagination progress
- Removal operations (when using --user-max)
- Final summary

## Error Handling

The script includes comprehensive error checking for:
- Invalid namespace format
- Token permissions
- API accessibility
- User removal operations
- File operations

## Support

For issues or questions:
- Check debug output for error messages
- Verify token permissions
- Ensure namespace path is correct
- Contact the maintainer @fsieverding if issues persist

## Notes

- User removal is irreversible - use --user-max carefully
- Large groups may take longer to process
- Token requires appropriate permissions for removal operations
- Always verify namespace path before execution

## Call:
`python3 ./GitLab_User_Analysis/GitLab_Com_user_list_by_activity.py --namespace "https://gitlab.com/gl-demo-ultimate-fsieverding" --token $GITLAB_PRIVATE_TOKEN`
`python3 ./GitLab_User_Analysis/GitLab_Com_user_list_by_activity.py --namespace "https://gitlab.com/groups/gl-demo-ultimate-fsieverding/" --token $GITLAB_PRIVATE_TOKEN`

`python3 ./GitLab_User_Analysis/GitLab_Com_user_list_by_activity.py --namespace "https://gitlab.com/gl-demo-ultimate-fsieverding" --token $GITLAB_PRIVATE_TOKEN --user-max 5`

`export GITLAB_PRIVATE_TOKEN="TOKEN"`


`python3 ./GitLab_User_Analysis/GitLab_Com_user_list_by_activity.py --namespace "https://gitlab.com/gitlab-com/" --token $GITLAB_PAT_TOKEN`

`export GITLAB_PAT_TOKEN="TOKEN"`
