#!/usr/bin/env python3
import sys
import argparse
from typing import List, Dict, Any
import gitlab
import requests
from urllib.parse import urlparse
from datetime import datetime, timedelta
import json
import os

def parse_arguments():
    parser = argparse.ArgumentParser(description="Fetch GitLab user contributions")
    parser.add_argument("--url", default="https://gitlab.com", help="GitLab server URL (default: https://gitlab.com)")
    parser.add_argument("--token", help="Personal Access Token (optional for gitlab.com)")
    parser.add_argument("--users", nargs="+", required=True, help="List of GitLab usernames")
    parser.add_argument("--days", type=int, default=30, help="Number of days to fetch contributions for (default: 30)")
    return parser.parse_args()

def validate_gitlab_connection(url, token):
    parsed_url = urlparse(url)
    if not parsed_url.scheme or not parsed_url.netloc:
        print(f"Error: Invalid URL format: {url}")
        sys.exit(1)

    if url != "https://gitlab.com":
        try:
            response = requests.get(f"{url}/api/v4/version", timeout=5)
            if response.status_code != 200:
                print(f"Error: Unable to reach GitLab server at {url}. Status code: {response.status_code}")
                sys.exit(1)
        except requests.exceptions.RequestException as e:
            print(f"Error: Unable to connect to GitLab server at {url}. {str(e)}")
            sys.exit(1)

    if token:
        gl = gitlab.Gitlab(url, private_token=token)
        try:
            gl.auth()
            print(f"Successfully connected to GitLab server: {url} (authenticated)")
        except Exception as e:
            print(f"Error: Unable to authenticate with GitLab server {url}. {str(e)}")
            sys.exit(1)
    else:
        if url != "https://gitlab.com":
            print("Error: Token is required for non-gitlab.com servers")
            sys.exit(1)
        gl = gitlab.Gitlab(url)
        print(f"Connected to GitLab server: {url} (unauthenticated)")
    
    return gl

def validate_users(gl, usernames):
    valid_users = []
    for username in usernames:
        try:
            user = gl.users.list(username=username)[0]
            valid_users.append(user)
            print(f"User '{username}' found.")
        except IndexError:
            print(f"Warning: User '{username}' not found. Skipping.")
        except Exception as e:
            print(f"Error: Unable to validate user '{username}'. {str(e)}")
    return valid_users

def get_user_contributions(gl, user, days):
    try:
        after_date = datetime.now() - timedelta(days=days)
        #Debug Print
        print(f"Debug: Fetching events for user {user.username} after {after_date.isoformat()}")
        events = user.events.list(all=True, after=after_date.isoformat())
        return events
    except gitlab.exceptions.GitlabAuthenticationError:
        print(f"Warning: Unable to fetch contributions for user {user.username}. Authentication required.")
        return []
    except Exception as e:
        print(f"Error: Unable to fetch contributions for user {user.username}. {str(e)}")
        return []

def get_event_link(event):
    base_url = event.manager.gitlab.url
    project_id = event.project_id
    
    # Debug Print
    print(f"# Debug Get Event: Event details - Project ID: {project_id}, Target Type: {event.target_type}, Target IID: {event.target_iid}, Base_URL: {base_url}, Event Note: \n {event.note if hasattr(event, 'note') else 'No note attribute'}\n")

    if event.target_type == 'Epic':
        return f"{base_url}/groups/{project_id}/-/epics/{event.target_iid}"
    elif event.target_type in ['Issue', 'MergeRequest', 'Commit']:
        return f"{base_url}/{project_id}/-/{event.target_type.lower()}s/{event.target_iid}"
    elif event.target_type == 'Note':
        if event.note.noteable_type == 'Commit':
            return f"{base_url}/{project_id}/-/commit/{event.note.commit_id}"
        else:
            return f"{base_url}/{project_id}/-/{event.note.noteable_type.lower()}s/{event.note.noteable_iid}#note_{event.note.id}"
    elif event.target_type == 'WorkItem':
        return f"{base_url}/{project_id}/-/work_items/{event.target_iid}"
    else:
        return "N/A"

def display_contributions(username, contributions):
    print(f"\nContributions for user: {username}")
    
    if not contributions:
        print("No contributions found or unable to access contribution data.")
        return

    for event in contributions:
        action = event.action_name
        target_type = event.target_type
        #link = get_event_link(event)
        link = 'DebugValue'
        #Debug Print
        print(f"### Debug Link: Event details - Action: {action}, Target Type: {target_type}, Link: {link}\n")
        if action in ['closed', 'opened'] and target_type in ['Issue', 'MergeRequest', 'WorkItem']:
            print(f"- {action.capitalize()} {target_type}: {link}")
        elif action == 'commented on' and target_type in ['Note', 'DiffNote']:
            noteable_type = event.note.noteable_type if hasattr(event, 'note') else 'Unknown'
            print(f"- Commented on {noteable_type}: {link}")
        elif action == 'accepted' and target_type == 'MergeRequest':
            print(f"- Accepted Merge request: {link}")
        elif action == 'pushed to':
            if hasattr(event, 'push_data') and 'commit_to' in event.push_data:
                commit_sha = event.push_data['commit_to']
                commit_link = f"{link}/commit/{commit_sha}"
                print(f"- Pushed commits: {commit_link}")
            else:
                print(f"- Pushed commits: {link}")
        elif action == 'pushed new':
            if hasattr(event, 'push_data') and 'commit_to' in event.push_data:
                commit_sha = event.push_data['commit_to']
                commit_link = f"{link}/commit/{commit_sha}"
                print(f"- Pushed commits: {commit_link}")
            else:
                print(f"- Pushed commits: {link}")
        else:
            print(f"- {action.capitalize()} {target_type}: {link}")

def display_contributions_raw(username, contributions):
    print(f"\nRaw Contributions Data for user: {username}")
    print("=" * 50)
    
    if not contributions:
        print("No contributions found or unable to access contribution data.")
        return

    serializable_contributions = []
    for event in contributions:
        event_dict = {k: v for k, v in event.attributes.items() if not k.startswith('_')}
        serializable_contributions.append(event_dict)
        
        print(f"Event ID: {event.id}")
        print(f"Project ID: {event.project_id}")
        print(f"Action: {event.action_name}")
        print(f"Target Type: {event.target_type}")
        print(f"Target Title: {event.target_title}")
        print(f"Created At: {event.created_at}")
        print("-" * 50)

    current_time = datetime.now().strftime("%Y%m%d_%H%M%S")
    filename = f"{username}_contributions_{current_time}.json"
    
    os.makedirs('output', exist_ok=True)
    
    file_path = os.path.join('output', filename)
    with open(file_path, 'w') as f:
        json.dump(serializable_contributions, f, indent=2, default=str)
    
    print(f"Raw contribution data has been written to: {file_path}")

def main():
    args = parse_arguments()
    gl = validate_gitlab_connection(args.url, args.token)
    valid_users = validate_users(gl, args.users)
    
    for user in valid_users:
        contributions = get_user_contributions(gl, user, args.days)
        display_contributions(user.username, contributions)
        display_contributions_raw(user.username, contributions)

if __name__ == "__main__":
    main()
