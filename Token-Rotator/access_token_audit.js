const axios= require("axios");
const TOKEN = "HERE_YOUR_ACCESS_TOKEN"

const getAccessTokens = async () => {
    const url = "https://URL_GITLAB/api/v4/personal_access_tokens";
    const response = await axios.get(url, {
        headers: {
            Authorization: `Bearer ${TOKEN}`,
        },
    });

    if (response.status !== 200) {
        throw new Error(response.statusText);
    }

    const accessTokens = [];
    const data = response.data;
    for (const token of data) {
        accessTokens.push({
            id: token.id,
            name: token.name,
            revoked: token.revoked,
            created_at: token.created_at,
            scopes: token.scopes,
            user_id: token.user_id,
            last_used_at: token.last_used_at,
            active: token.active,
            expires_at: token.expires_at,
        });
    }

    return accessTokens;
};

const getUserName = async (userId) => {
    const url = `https://URL_GITLAB/api/v4/users/${userId}`;
    const response = await axios.get(url, {
        headers: {
            Authorization: `Bearer ${TOKEN}`,
        },
    });

    if (response.status !== 200) {
        throw new Error(response.statusText);
    }

    const data = response.data;
    return data.name;
};

const main = async () => {
    const accessTokens = await getAccessTokens();
    const csvData = [];
    const headers = ["User", "Created At", "Expires At"];
    csvData.push(headers);
    for (const token of accessTokens) {
        const userName = await getUserName(token.user_id);
        console.log(userName,token.scopes, token.created_at, token.expires_at)
    }
};

main();