import gitlab
import csv
import argparse
import requests

# Set up argparse
parser = argparse.ArgumentParser(description='GitLab Access Token Exporter')
parser.add_argument('--url', help='GitLab server URL', required=True)
parser.add_argument('--token', help='Private access token', required=True)
args = parser.parse_args()

# Configuration
gitlab_url = args.url
private_token = args.token

# Check if URL is accessible
try:
    response = requests.get(gitlab_url)
    response.raise_for_status()
except requests.exceptions.RequestException as e:
    raise SystemExit(f"Error: Unable to access URL '{gitlab_url}'. {e}")

# Initialize a GitLab instance and test token
try:
    gl = gitlab.Gitlab(gitlab_url, private_token=private_token)
    gl.auth()
except gitlab.GitlabAuthenticationError:
    raise SystemExit("Error: Authentication failed. Check your private token.")
except gitlab.GitlabError as e:
    raise SystemExit(f"Error: {e}")
import gitlab
import csv

# Configuration
gitlab_url = 'https://gitlab.com'  # Your GitLab server URL
private_token = 'YOUR_PRIVATE_TOKEN'  # Your private access token

# Initialize a GitLab instance
gl = gitlab.Gitlab(gitlab_url, private_token=private_token)

def get_all_projects():
    """Retrieve all projects."""
    return gl.projects.list(all=True)

def get_all_groups():
    """Retrieve all groups and subgroups."""
    groups = gl.groups.list(all=True)
    all_groups = []
    for group in groups:
        all_groups.append(group)
        subgroups = group.subgroups.list(all=True)
        all_groups.extend(subgroups)
    return all_groups

def get_project_access_tokens(project):
    """Retrieve access tokens for a given project."""
    return project.accesstokens.list()

def get_group_access_tokens(group):
    """Retrieve access tokens for a given group."""
    return group.accesstokens.list()

def export_to_csv(filename, data):
    """Export data to a CSV file."""
    with open(filename, mode='w', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        writer.writerow(['Type', 'ID', 'Name', 'Access Token'])
        for item in data:
            writer.writerows(item)

# Fetch data
projects = get_all_projects()
groups = get_all_groups()

project_data = [['Project', p.id, p.name, token.token for p in projects for token in get_project_access_tokens(p)]]
group_data = [['Group', g.id, g.name, token.token for g in groups for token in get_group_access_tokens(g)]]

# Combine and export data
export_to_csv('gitlab_access_tokens.csv', project_data + group_data)

print("Export completed.")
