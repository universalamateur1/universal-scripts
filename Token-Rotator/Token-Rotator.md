# Token Rotator

Goal is to have a script finding all Tokens in the users resposibility or parsing those from a source,
then to get the Tokens, refresh them and store those in the same location.

## Token Management

Token management is crucial in providing authentication and authorization within various systems and subsystems used by GitLab. The [token overview](https://docs.gitlab.com/ee/security/token_overview.html#gitlab-token-overview) will assist in identifying the tokens used in GitLab. Refer to the [Token Management Standard](https://about.gitlab.com/handbook/security/token-management-standard.html) for approved token usage and distribution.

## BEst Practises for GitLab Tokens

1. Sharing and distribution of tokens will be limited, and must include documentation, and identity verification of who the token is being distributed to
2. Token names will include the Group/Project name, and the creation date (YYYYMMDD)
3. Tokens will be chosen with the least Privileged necessary Sort: Deploy Key - Deploy Token - Project Access Token - Group Access Token - Personal Access Token
4. Tokens will be created with the minimum role and scope necessary to perform the desired task(s)
5. Storage of a token must be secure. It needs to be encrypted. Recommended methods include usage of a security vault or a key management system. For cloud or self-managed environments this can include Hashicorp Vaults Pro, Amazon KMS or Google Secret Manager.
6. Never reuse an existing token for your own automation, as this makes it harder to track what a token is used for. It also increases the number of changes required if the token is revoked.

## First Functionality

Have a PAT in the Project CICD Variables and refresh this plus store it again.

## Resources

* [Dynamic Secrets MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/7569)
* [Spike - Token rotation](https://gitlab.com/gitlab-org/gitlab/-/issues/387606)
* [Improve tokens management and rotation](https://gitlab.com/gitlab-org/gitlab/-/issues/381399)
* [MR: PAT rotation via API](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/113125)
* [GitLab Engineering Guidelines for automation and access tokens](https://about.gitlab.com/handbook/engineering/automation/)
* [GitLab Service accounts](https://gitlab.com/groups/gitlab-org/-/epics/6777)
* [Token Hunter](https://gitlab.com/gitlab-com/gl-security/threatmanagement/redteam/redteam-public/token-hunter)
* [Advanced Search Token Hunter](https://gitlab.com/gitlab-com/gl-security/appsec/advanced-search-token-hunter)
