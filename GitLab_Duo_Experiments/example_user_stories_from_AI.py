def calculate_total_price(items, discount_percentage):
    subtotal = sum(item['price'] for item in items)
    discount = subtotal * (discount_percentage / 100)
    total = subtotal - discount
    return total