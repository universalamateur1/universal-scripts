#!/usr/bin/env python3

"""
GitLab User Add-on Information Extractor

This script connects to a GitLab Self-Managed instance, retrieves user information
via GraphQL API, and generates a CSV report of users with specific add-ons
(DUO_ENTERPRISE or CODE_SUGGESTIONS).

Usage:
    python GitLab_Duo_user_list.py --url "https://gitlab.example.com" --token "your-admin-token"
    
Environment variables:
    GITLAB_PRIVATE_TOKEN: Can be used to store the GitLab admin token

Dependencies:
    - requests
    - argparse (stdlib)
    - csv (stdlib)
    - datetime (stdlib)
    - urllib.parse (stdlib)
"""

import argparse
import csv
import json
import re
import requests
import sys
from datetime import datetime
from urllib.parse import urlparse

def validate_gitlab_url(url):
    """
    Validate and normalize the GitLab instance URL.
    
    Ensures the URL has a proper scheme (http/https) and netloc (domain).
    Normalizes the URL by removing trailing slashes and ensuring proper format.
    
    Args:
        url (str): The input URL to validate
    
    Returns:
        str: The normalized GitLab instance URL
    
    Raises:
        ValueError: If the URL format is invalid
    """
    try:
        result = urlparse(url)
        if not all([result.scheme, result.netloc]):
            raise ValueError("Invalid URL format")
        return f"{result.scheme}://{result.netloc}"
    except Exception as e:
        raise ValueError(f"Invalid GitLab instance URL: {str(e)}")

def validate_token(url, token):
    """
    Validate if the provided token has admin privileges.
    
    Tests the token by attempting to access an admin-only endpoint
    (/api/v4/application/settings).
    
    Args:
        url (str): The GitLab instance URL
        token (str): The GitLab API token to validate
    
    Returns:
        bool: True if token is valid and has admin privileges
    
    Raises:
        ValueError: If token is invalid or lacks required permissions
    """
    print("[DEBUG] Validating token permissions...")
    
    headers = {
        'Authorization': f'Bearer {token}',
        'Content-Type': 'application/json',
    }
    
    try:
        # Try to access admin-only endpoint to verify permissions
        response = requests.get(
            f"{url}/api/v4/application/settings",
            headers=headers,
            timeout=10
        )
        if response.status_code != 200:
            raise ValueError("Invalid token or insufficient permissions")
        return True
    except requests.exceptions.RequestException as e:
        raise ValueError(f"Failed to validate token: {str(e)}")

def filter_eligible_users(data):
    """
    Filter users with DUO_ENTERPRISE or CODE_SUGGESTIONS add-ons.
    
    Processes the GraphQL query results and extracts users with specific add-ons.
    Also performs validation of received data against reported counts.
    
    Args:
        data (dict): The GraphQL query results
    
    Returns:
        list: List of dictionaries containing eligible user information
    """
    print("[DEBUG] Filtering eligible users...")
    
    eligible_users = []
    if not data or 'data' not in data:
        return eligible_users
    
    # Extract counts for validation
    total_count = data['data']['selfManagedAddOnEligibleUsers']['count']
    users = data['data']['selfManagedAddOnEligibleUsers']['nodes']
    total_users_received = len(users)
    
    print(f"[DEBUG] Total users reported by GitLab: {total_count}")
    print(f"[DEBUG] Total user records received: {total_users_received}")
    
    # Validate data consistency
    if total_count != total_users_received:
        print(f"[WARNING] Discrepancy in user counts: GitLab reports {total_count} users but received {total_users_received} records")
    
    # Filter users based on add-on assignments
    for user in users:
        if user['addOnAssignments']['nodes']:
            addon_name = user['addOnAssignments']['nodes'][0]['addOnPurchase']['name']
            if addon_name in ['DUO_ENTERPRISE', 'CODE_SUGGESTIONS']:
                eligible_users.append({
                    'username': user['username'],
                    'name': user['name'],
                    'publicEmail': user['publicEmail'] or '',
                    'lastActivityOn': user['lastActivityOn'] or '',
                    'addOnType': addon_name
                })
    
    print(f"[DEBUG] Users with eligible add-ons found: {len(eligible_users)}")
    return eligible_users

def get_instance_name(url):
    """
    Extract a clean instance name from the URL for the filename.
    
    Args:
        url (str): The GitLab instance URL
    
    Returns:
        str: Cleaned instance name suitable for filename
    """
    parsed = urlparse(url)
    return parsed.netloc.replace('.', '_')

def write_to_csv(users, gitlab_url):
    """
    Write filtered user data to CSV file.
    
    Creates a CSV file with a timestamp and instance name in the filename.
    
    Args:
        users (list): List of user dictionaries to write
        gitlab_url (str): The GitLab instance URL
    
    Returns:
        str: Name of the created CSV file
    
    Raises:
        ValueError: If writing to CSV fails
    """
    instance_name = get_instance_name(gitlab_url)
    timestamp = datetime.now().strftime('%Y_%m_%d_%H:%M')
    filename = f"gitlab_user_addons_{instance_name}_{timestamp}.csv"
    
    print(f"[DEBUG] Writing data to {filename}...")
    
    headers = ['Username', 'Name', 'Public Email', 'Last Activity Date', 'Add-On Type']
    
    try:
        with open(filename, 'w', newline='', encoding='utf-8') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=headers)
            writer.writeheader()
            for user in users:
                writer.writerow({
                    'Username': user['username'],
                    'Name': user['name'],
                    'Public Email': user['publicEmail'],
                    'Last Activity Date': user['lastActivityOn'],
                    'Add-On Type': user['addOnType']
                })
        return filename
    except IOError as e:
        raise ValueError(f"Failed to write CSV file: {str(e)}")
def execute_graphql_query(url, token, addon_id, addon_name):
    """
    Execute the GraphQL query to get user data with pagination support.
    
    Args:
        url (str): The GitLab instance URL
        token (str): The GitLab API token
        addon_id (str): The ID of the add-on to query
        addon_name (str): The name of the add-on (for debugging)
    
    Returns:
        dict: Combined results from all pagination requests
    
    Raises:
        ValueError: If the query fails or returns errors
    """
    print(f"[DEBUG] Executing GraphQL query for add-on: {addon_name}")
    print(f"[DEBUG] Add-on ID: {addon_id}")
    
    headers = {
        'Authorization': f'Bearer {token}',
        'Content-Type': 'application/json',
    }
    
    # Updated query with correct type annotation for addOnPurchaseIds
    query = """
    query GetEligableAddOnUsersWithData($after: String, $addonId: [GitlabSubscriptionsAddOnPurchaseID!]!) {
      selfManagedAddOnEligibleUsers(
        addOnType: CODE_SUGGESTIONS,
        first: 100,
        after: $after
      ) {
        count
        pageInfo {
          endCursor
          hasNextPage
        }
        nodes {
          id
          username
          name
          publicEmail
          lastActivityOn
          addOnAssignments(addOnPurchaseIds: $addonId) {
            nodes {
              addOnPurchase {
                name
              }
            }
          }
        }
      }
    }
    """
    
    all_users_data = None
    cursor = None
    page_count = 0
    
    try:
        while True:
            page_count += 1
            print(f"[DEBUG] Fetching page {page_count} for {addon_name}...")
            
            # Prepare variables for pagination and addon ID
            variables = {
                "after": cursor,
                "addonId": [addon_id]
            }
            
            print(f"[DEBUG] Query variables: {json.dumps(variables, indent=2)}")
            
            response = requests.post(
                f"{url}/api/graphql",
                headers=headers,
                json={'query': query, 'variables': variables},
                timeout=30
            )
            
            if response.status_code != 200:
                print(f"[ERROR] HTTP Status Code: {response.status_code}")
                print(f"[ERROR] Response Headers: {json.dumps(dict(response.headers), indent=2)}")
                print(f"[ERROR] Response Body: {response.text}")
                raise ValueError(f"GraphQL query failed with status code: {response.status_code}")
            
            result = response.json()
            
            if 'errors' in result:
                print("[ERROR] GraphQL Query Error Details:")
                for error in result['errors']:
                    print(f"  Message: {error.get('message')}")
                    print(f"  Location: {error.get('locations')}")
                    print(f"  Path: {error.get('path')}")
                    print(f"  Extensions: {json.dumps(error.get('extensions', {}), indent=2)}")
                raise ValueError(f"GraphQL query returned errors: {result['errors']}")
            
            if all_users_data is None:
                all_users_data = result
            else:
                all_users_data['data']['selfManagedAddOnEligibleUsers']['nodes'].extend(
                    result['data']['selfManagedAddOnEligibleUsers']['nodes']
                )
            
            page_info = result['data']['selfManagedAddOnEligibleUsers']['pageInfo']
            total_count = result['data']['selfManagedAddOnEligibleUsers']['count']
            current_count = len(all_users_data['data']['selfManagedAddOnEligibleUsers']['nodes'])
            
            print(f"[DEBUG] Retrieved {current_count} of {total_count} total users for {addon_name}")
            
            if not page_info['hasNextPage']:
                break
                
            cursor = page_info['endCursor']
            
        print(f"[DEBUG] Completed data retrieval for {addon_name} in {page_count} pages")
        return all_users_data
        
    except requests.exceptions.RequestException as e:
        print(f"[ERROR] Network or request error: {str(e)}")
        raise ValueError(f"Failed to execute GraphQL query: {str(e)}")
    except json.JSONDecodeError as e:
        print(f"[ERROR] Failed to parse JSON response: {str(e)}")
        print(f"[ERROR] Raw response: {response.text}")
        raise ValueError("Invalid JSON response from server")
    except Exception as e:
        print(f"[ERROR] Unexpected error: {str(e)}")
        raise

def execute_addon_query(url, token):
    """
    Execute GraphQL query to get available add-ons.
    
    Args:
        url (str): The GitLab instance URL
        token (str): The GitLab API token
    
    Returns:
        list: List of dictionaries containing add-on information
    """
    print("[DEBUG] Querying available add-ons...")
    
    headers = {
        'Authorization': f'Bearer {token}',
        'Content-Type': 'application/json',
    }
    
    query = """
    query GettPurchasedAddOns {
      addOnPurchases {
        id
        name
        assignedQuantity
        purchasedQuantity
      }
    }
    """
    
    try:
        response = requests.post(
            f"{url}/api/graphql",
            headers=headers,
            json={'query': query},
            timeout=30
        )
        
        if response.status_code != 200:
            print(f"[ERROR] Add-on query failed with status {response.status_code}")
            print(f"[ERROR] Response: {response.text}")
            raise ValueError(f"Add-on query failed with status code: {response.status_code}")
        
        result = response.json()
        
        if 'errors' in result:
            print("[ERROR] Add-on Query Error Details:")
            for error in result['errors']:
                print(f"  Message: {error.get('message')}")
                print(f"  Location: {error.get('locations')}")
                print(f"  Path: {error.get('path')}")
            raise ValueError(f"Add-on query returned errors: {result['errors']}")
            
        addons = result['data']['addOnPurchases']
        print(f"[DEBUG] Found {len(addons)} add-ons")
        
        for addon in addons:
            print(f"[DEBUG] Add-on: {addon['name']}, ID: {addon['id']}")
            print(f"[DEBUG] Assigned: {addon['assignedQuantity']}, Purchased: {addon['purchasedQuantity']}")
            print(f"[DEBUG] Raw add-on data: {json.dumps(addon, indent=2)}")
            
        return addons
        
    except requests.exceptions.RequestException as e:
        print(f"[ERROR] Network error during add-on query: {str(e)}")
        raise ValueError(f"Failed to execute add-on query: {str(e)}")

def main():
    """
    Main function to orchestrate the GitLab user add-on information extraction.
    
    Handles argument parsing, executes the data retrieval and processing pipeline,
    and manages error handling.
    """
    parser = argparse.ArgumentParser(
        description="Extract GitLab user add-on information and save to CSV"
    )
    parser.add_argument(
        "--url",
        required=True,
        help="GitLab instance URL (e.g., https://gitlab.example.com)"
    )
    parser.add_argument(
        "--token",
        required=True,
        help="Personal Access Token with admin permissions"
    )
    args = parser.parse_args()

    try:
        # Validate and normalize URL
        gitlab_url = validate_gitlab_url(args.url)
        print(f"[DEBUG] Connecting to GitLab instance: {gitlab_url}")
        
        # Validate token and admin permissions
        validate_token(gitlab_url, args.token)
        print("[DEBUG] Token validation successful")
        
        # Get available add-ons
        addons = execute_addon_query(gitlab_url, args.token)
        print(f"[DEBUG] Processing {len(addons)} add-ons")
        
        all_eligible_users = []
        
        # Process each add-on
        for addon in addons:
            print(f"\n[DEBUG] Processing add-on: {addon['name']}")
            
            # Execute GraphQL query with pagination for each add-on
            query_result = execute_graphql_query(
                gitlab_url,
                args.token,
                addon['id'],
                addon['name']
            )
            
            # Filter eligible users for this add-on
            eligible_users = filter_eligible_users(query_result)
            all_eligible_users.extend(eligible_users)
            
            print(f"[DEBUG] Found {len(eligible_users)} users for {addon['name']}")
        
        # Write results to CSV
        output_file = write_to_csv(all_eligible_users, gitlab_url)
        
        # Print summary
        print("\n[SUMMARY]")
        print(f"Total add-ons processed: {len(addons)}")
        print(f"Total eligible users found: {len(all_eligible_users)}")
        print(f"\nResults have been saved to {output_file}")
        
    except ValueError as e:
        print(f"Error: {str(e)}")
        sys.exit(1)
    except Exception as e:
        print(f"An unexpected error occurred: {str(e)}")
        sys.exit(1)

if __name__ == "__main__":
    main()
