# Universal-Scripts

Collection of useful Scrpts

## Local testing - Using the GitLab Scripts

### Venv

```bash
python3 -m venv ~/git/universal-scripts/.
source ~/git/universal-scripts/./bin/activate
```

- Confirm `which python`

### Pip

```bash
python3 -m pip install --upgrade pip
python3 -m pip --version
```

### Reqs

```bash
`python3 -m pip freeze >| ./GitLab_User_Analysis/requirements.txt`
```

```bash
python3 -m pip install -r ./GitLab_User_Analysis/requirements.txt
```
