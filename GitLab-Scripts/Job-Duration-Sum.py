#!/usr/bin/env bash
import requests
import datetime
import argparse

# Function to fetch jobs
def fetch_jobs(url, headers, query, cursor=None):
    variables = {'cursor': cursor}
    response = requests.post(url, headers=headers, json={'query': query, 'variables': variables})
    response.raise_for_status()  # Raise an exception for HTTP errors
    return response.json()

def check_gitlab_instance(url):
    try:
        response = requests.get(url)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:
        raise SystemExit(f"Error: Unable to reach the GitLab instance at {url}. Please check the URL and try again. {e}")

def check_admin_token(url, headers):
    try:
        response = requests.get(f"{url}/api/v4/user", headers=headers)
        response.raise_for_status()
        user_data = response.json()
        if not user_data.get('is_admin', False):
            raise SystemExit("Error: The provided token does not have admin permissions.")
    except requests.exceptions.RequestException as e:
        raise SystemExit(f"Error: Invalid token or unable to authenticate. {e}")
    return user_data

def main(gitlab_url, access_token):
    # Check if the GitLab instance is reachable
    check_gitlab_instance(gitlab_url)

    # Define headers for authentication
    headers = {'Authorization': f'Bearer {access_token}'}

    # Check if the token is valid and has admin permissions
    print(check_admin_token(gitlab_url, headers))

    # GraphQL endpoint
    url = f"{gitlab_url}/api/graphql"

    # Define the GraphQL query
    query = """
    query($cursor: String) {
      jobs(first: 100, after: $cursor) {
        pageInfo {
          endCursor
          hasNextPage
        }
        nodes {
          id
          name
          status
          createdAt
          startedAt
          finishedAt
          duration
          refName
          project {
            id
            name
            namespace {
              fullPath
            }
          }
        }
      }
    }
    """

    # Date 28 days ago
    date_28_days_ago = datetime.datetime.now() - datetime.timedelta(days=2)
    # Debug Printstatement
    print(f'Date until the data will be printed{date_28_days_ago}')
    # Fetch jobs and filter them
    cursor = None
    all_jobs = []
    should_continue = True

    while should_continue:
        result = fetch_jobs(url, headers, query, cursor)
        jobs = result['data']['jobs']['nodes']
 
        for job in jobs:
            created_at = datetime.datetime.strptime(job['createdAt'], '%Y-%m-%dT%H:%M:%SZ')
            # Debug Printstatement
            print(f'The date the latest job was created {created_at} is smaller than {date_28_days_ago}')
            if created_at >= date_28_days_ago:
                all_jobs.append(job)
            else:
                should_continue = False
                break

        if result['data']['jobs']['pageInfo']['hasNextPage'] and should_continue:
            cursor = result['data']['jobs']['pageInfo']['endCursor']
        else:
            break

    # Print filtered jobs
    for job in all_jobs:
        print(job)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Fetch GitLab CI jobs from the last 28 days.')
    parser.add_argument('--instance-url', type=str, required=True, help='The URL of the GitLab instance')
    parser.add_argument('--admin-token', type=str, required=True, help='The GitLab access token')

    args = parser.parse_args()
    main(args.instance_url, args.admin_token)