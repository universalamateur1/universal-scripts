import requests
import datetime
import json

# URL of the markdown file
url = "https://gitlab.com/gitlab-org/gitlab/-/raw/master/doc/user/ai_features.md"

# Fetch the markdown content
response = requests.get(url)
lines = response.text.split('\n')

# Find the start of the table based on the headers
start = [i for i, line in enumerate(lines) if '| Goal |' in line][0]

# Find the end of the table as the first blank line after the last content line
end = start
while end < len(lines) and '|' in lines[end]:
    end += 1

# Process the table
table_dicts = []
for line in lines[start + 2:end]:  # Skip header underline
    if line.strip() and '|' in line:
        parts = line.split('|')
        if len(parts) >= 4:  # Check for at least 3 columns in the split result
            # Remove HTML tag <br> from the strings and strip whitespaces
            goal = parts[1].replace('<br>', '').strip()
            feature = parts[2].replace('<br>', '').strip()
            tier_offering_status = parts[3].replace('<br>', '').strip()

            # Extract Tier, Offering, and Status using their specific markers
            tier = tier_offering_status.split('**Tier:**')[1].split('**Offering:**')[0].replace('<br>', '').strip()
            offering = tier_offering_status.split('**Offering:**')[1].split('**Status:**')[0].replace('<br>', '').strip()
            status = tier_offering_status.split('**Status:**')[1].replace('<br>', '').strip()

            # Determine the numerical value for offering based on its contents
            #"offering" = "GitLab.com" set 1
            #"offering" = "GitLab Dedicated" set 2
            #"offering" = "Self-managed" set 4
            #"offering" = "GitLab.com" & "GitLab Dedicated" set 3
            #"offering" = "Self-managed" & "GitLab.com" set 5
            #"offering" = "Self-managed" & "GitLab Dedicated" set 6
            #"offering" = "GitLab.com" & "Self-managed" & "GitLab Dedicated" set 7
            offerings = offering.split(", ")
            if set(offerings) == {"GitLab.com", "Self-managed", "GitLab Dedicated"}:
                offering_value = 7
            elif offerings == ["GitLab.com"]:
                offering_value = 1
            elif offerings == ["GitLab Dedicated"]:
                offering_value = 2
            elif offerings == ["Self-managed"]:
                offering_value = 4
            elif set(offerings) == {"GitLab.com", "GitLab Dedicated"}:
                offering_value = 3
            elif set(offerings) == {"Self-managed", "GitLab Dedicated"}:
                offering_value = 6
            elif set(offerings) == {"Self-managed", "GitLab.com"}:
                offering_value = 5
            else:
                offering_value = 0  # Default if no known combination is matched

            # Append the Dictionary to the list
            table_dicts.append({
                "Goal": goal,
                "Feature": feature,
                "Tier": tier,
                "Offering": offering,
                "Offering_value": offering_value,
                "Status": status
            })

# Print each dictionary element in a new block
for item in table_dicts:
    print("{")
    for key, value in item.items():
        print(f"    {key}: {value}")
    print("}\n")

# Get the current datetime to use in the filename
now = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")
filename = now + "_GitLab-Duo_Features.md"

# Write the data to a Markdown file
with open(filename, 'w') as file:
    file.write("# GitLab Duo Features\n\n")  # Title for the markdown file
    for item in table_dicts:
        file.write("## Feature Block\n")
        for key, value in item.items():
            file.write(f"- **{key}**: {value}\n")
        file.write("\n")  # Add a space between blocks

print(f"Data written to {filename}")

# Generate JavaScript file
js_filename = now + 'gitlab_features.js'
with open(js_filename, 'w') as js_file:
    js_file.write(
        f"""
        var app = new Vue({{
            el: '#app',
            data: {{
                features: {json.dumps(table_dicts)},
                filterOffering: '',
                filterStatus: '',
                filterTier: ''
            }},
            computed: {{
                filteredFeatures: function () {{
                    return this.features.filter(feature => {{
                        return (this.filterOffering === '' || feature.Offering.includes(this.filterOffering)) &&
                               (this.filterStatus === '' || feature.Status === this.filterStatus) &&
                               (this.filterTier === '' || feature.Tier.includes(this.filterTier));
                    }});
                }}
            }}
        }});
        """
    )

# HTML Content
html_content = f"""
<!DOCTYPE html>
<html>
<head>
    <title>GitLab Features</title>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
</head>
<body>
    <div id="app">
        <select v-model="filterOffering">
            <option value="">All Offerings</option>
            <option value="GitLab.com">GitLab.com</option>
            <option value="Self-managed">Self-managed</option>
            <option value="GitLab Dedicated">GitLab Dedicated</option>
        </select>
        <select v-model="filterStatus">
            <option value="">All Status</option>
            <option value="Generally Available">Generally Available</option>
            <option value="Beta">Beta</option>
            <option value="Experiment">Experiment</option>
        </select>
        <select v-model="filterTier">
            <option value="">All Tiers</option>
            <option value="Ultimate">Ultimate</option>
            <option value="Premium">Premium</option>
            <option value="Free">Free</option>
        </select>
        <ul>
            <li v-for="feature in filteredFeatures">
                {{ feature.Goal }} - {{ feature.Feature }} - {{ feature.Tier }} - {{ feature.Offering }} - {{ feature.Status }}
            </li>
        </ul>
    </div>
    <script src="{js_filename}"></script>
</body>
</html>
"""

# Save HTML file
html_filename = now + 'gitlab_features.html'
with open(html_filename, 'w') as html_file:
    html_file.write(html_content)

print(f"Web page created: {html_filename}")
