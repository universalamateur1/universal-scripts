import dns.resolver

domain = "git.mbos.cloud"

# Check A record
a_records = dns.resolver.resolve(domain, 'A')
for rdata in a_records:
    print(f"A Record: {rdata.address}")

# Check CAA record
try:
    caa_records = dns.resolver.resolve(domain, 'CAA')
    for rdata in caa_records:
        print(f"CAA Record: {rdata.to_text()}")
except dns.resolver.NoAnswer:
    print("No CAA record found")
