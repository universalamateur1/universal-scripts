#!/usr/bin/env bash

# Description of the script
# Getting the results out of GitLab relase RSS Feed
: <<'DESCRIPTION'
ToCOme
DESCRIPTION

# Main script logic
today=$(date +%Y-%m-%d) # Get today's date in YYYY-MM-DD format
filename="${today}-Major-GitLab-Releases.md" # Construct the filename

curl -s https://about.gitlab.com/all-releases.xml | \
xmlstarlet sel -N atom="http://www.w3.org/2005/Atom" -t -c "/atom:feed/atom:entry" -nl | \
awk '
BEGIN { RS="<entry"; FS="\n"; entry="" }
{
  title=""; id=""; release="";
  for (i = 1; i <= NF; i++) {
    if ($i ~ /<release>/) {
      release=$i;
      gsub("<[^>]*>", "", release);
    }
    if ($i ~ /<title>/) {
      title=$i;
      gsub("<[^>]*>", "", title);
    }
    if ($i ~ /<id>/) {
      id=$i;
      gsub("<[^>]*>", "", id);
    }
  }
  if (release ~ /^[0-9]{2}\.[0-9]+$/) {
    print "- [" title "](" id ")"
  }
}
' > "$filename"


# Exit script with an optional exit code
exit 0
