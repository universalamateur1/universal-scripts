# GitLab Duo Features

## Feature Block
- **Goal**: Helps you write code more efficiently by showing code suggestions as you type.<i class="fa fa-youtube-play youtube" aria-hidden="true"></i> [Watch overview](https://www.youtube.com/watch?v=hCAyCTacdAQ)
- **Feature**: [Code Suggestions](project/repository/code_suggestions/index.md)
- **Tier**: Premium and Ultimate with [GitLab Duo Pro](../subscriptions/subscription-add-ons.md)
- **Offering**: GitLab.com, Self-managed, GitLab Dedicated
- **Offering_value**: 7
- **Status**: Generally Available

## Feature Block
- **Goal**: Processes and generates text and code in a conversational manner. Helps you quickly identify useful information in large volumes of text in issues, epics, code, and GitLab documentation.
- **Feature**: [Chat](gitlab_duo_chat.md)
- **Tier**: Freely available for Premium and Ultimate for a limited time
- **Offering**: GitLab.com, Self-managed, GitLab Dedicated
- **Offering_value**: 7
- **Status**: Generally Available

## Feature Block
- **Goal**: Helps you discover or recall Git commands when and where you need them.
- **Feature**: [Git suggestions](../editor_extensions/gitlab_cli/index.md#gitlab-duo-commands)
- **Tier**: Freely available for Ultimate for a limited timeIn the future, will require Ultimate with [GitLab Duo Enterprise](../subscriptions/subscription-add-ons.md)
- **Offering**: GitLab.com
- **Offering_value**: 1
- **Status**: Experiment

## Feature Block
- **Goal**: Assists with quickly getting everyone up to speed on lengthy conversations to help ensure you are all on the same page.  <i class="fa fa-youtube-play youtube" aria-hidden="true"></i> [Watch overview](https://www.youtube.com/watch?v=IcdxLfTIUgc)
- **Feature**: [Discussion summary](#summarize-issue-discussions-with-discussion-summary)
- **Tier**: Freely available for Ultimate for a limited timeIn the future, will require [GitLab Duo Enterprise](../subscriptions/subscription-add-ons.md)
- **Offering**: GitLab.com
- **Offering_value**: 1
- **Status**: Experiment

## Feature Block
- **Goal**: Generates issue descriptions.
- **Feature**: [Issue description generation](#summarize-an-issue-with-issue-description-generation)
- **Tier**: Freely available for Ultimate for a limited timeIn the future, will require Ultimate with [GitLab Duo Enterprise](../subscriptions/subscription-add-ons.md)
- **Offering**: GitLab.com
- **Offering_value**: 1
- **Status**: Experiment

## Feature Block
- **Goal**: Automates repetitive tasks and helps catch bugs early. <i class="fa fa-youtube-play youtube" aria-hidden="true"></i> [Watch overview](https://www.youtube.com/watch?v=g6MS1JsRWgs)
- **Feature**: [Test generation](gitlab_duo_chat.md#write-tests-in-the-ide)
- **Tier**: Freely available for Premium and Ultimate for a limited timeIn the future, will require Premium or Ultimate with [GitLab Duo Pro](../subscriptions/subscription-add-ons.md)
- **Offering**: GitLab.com, Self-managed, GitLab Dedicated
- **Offering_value**: 7
- **Status**: Beta

## Feature Block
- **Goal**: Generates a description for the merge request based on the contents of the template.
- **Feature**: [Merge request template population](project/merge_requests/ai_in_merge_requests.md#fill-in-merge-request-templates)
- **Tier**: Freely available for Ultimate for a limited timeIn the future, will require Ultimate with [GitLab Duo Enterprise](../subscriptions/subscription-add-ons.md)
- **Offering**: GitLab.com
- **Offering_value**: 1
- **Status**: Experiment

## Feature Block
- **Goal**: Assists in creating faster and higher-quality reviews by automatically suggesting reviewers for your merge request. <i class="fa fa-youtube-play youtube" aria-hidden="true"></i> [Watch overview](https://www.youtube.com/watch?v=ivwZQgh4Rxw)
- **Feature**: [Suggested Reviewers](project/merge_requests/reviews/index.md#gitlab-duo-suggested-reviewers)
- **Tier**: Ultimate
- **Offering**: GitLab.com
- **Offering_value**: 1
- **Status**: Generally Available

## Feature Block
- **Goal**: Efficiently communicates the impact of your merge request changes.
- **Feature**: [Merge request summary](project/merge_requests/ai_in_merge_requests.md#summarize-merge-request-changes)
- **Tier**: Freely available for Ultimate for a limited timeIn the future, will require Ultimate with [GitLab Duo Enterprise](../subscriptions/subscription-add-ons.md)
- **Offering**: GitLab.com
- **Offering_value**: 1
- **Status**: Beta

## Feature Block
- **Goal**: Helps ease merge request handoff between authors and reviewers and help reviewers efficiently understand suggestions.
- **Feature**: [Code review summary](project/merge_requests/ai_in_merge_requests.md#summarize-my-merge-request-review)
- **Tier**: Freely available for Ultimate for a limited timeIn the future, will require Ultimate with [GitLab Duo Enterprise](../subscriptions/subscription-add-ons.md)
- **Offering**: GitLab.com
- **Offering_value**: 1
- **Status**: Experiment

## Feature Block
- **Goal**: Helps you remediate vulnerabilities more efficiently, boost your skills, and write more secure code. <i class="fa fa-youtube-play youtube" aria-hidden="true"></i> [Watch overview](https://www.youtube.com/watch?v=6sDf73QOav8)
- **Feature**: [Vulnerability explanation](application_security/vulnerabilities/index.md#explaining-a-vulnerability)
- **Tier**: Freely available for Ultimate for a limited timeIn the future, will require Ultimate with [GitLab Duo Enterprise](../subscriptions/subscription-add-ons.md)
- **Offering**: GitLab.com
- **Offering_value**: 1
- **Status**: Beta

## Feature Block
- **Goal**: Generates a merge request containing the changes required to mitigate a vulnerability.
- **Feature**: [Vulnerability resolution](application_security/vulnerabilities/index.md#vulnerability-resolution)
- **Tier**: Freely available for Ultimate for a limited timeIn the future, will require Ultimate with [GitLab Duo Enterprise](../subscriptions/subscription-add-ons.md)
- **Offering**: GitLab.com
- **Offering_value**: 1
- **Status**: Experiment

## Feature Block
- **Goal**: Helps you understand code by explaining it in English language. <i class="fa fa-youtube-play youtube" aria-hidden="true"></i> [Watch overview](https://www.youtube.com/watch?v=1izKaLmmaCA)
- **Feature**: [Code explanation](#explain-code-in-the-web-ui-with-code-explanation)
- **Tier**: Freely available for Premium and Ultimate for a limited timeIn the future, will require Premium or Ultimate with [GitLab Duo Pro](../subscriptions/subscription-add-ons.md)
- **Offering**: GitLab.com
- **Offering_value**: 1
- **Status**: Experiment

## Feature Block
- **Goal**: Assists you in determining the root cause for a pipeline failure and failed CI/CD build.
- **Feature**: [Root cause analysis](#root-cause-analysis)
- **Tier**: Freely available for Ultimate for a limited timeIn the future, will require Ultimate with [GitLab Duo Enterprise](../subscriptions/subscription-add-ons.md)
- **Offering**: GitLab.com
- **Offering_value**: 1
- **Status**: Experiment

## Feature Block
- **Goal**: Assists you with predicting productivity metrics and identifying anomalies across your software development lifecycle.
- **Feature**: [Value stream forecasting](#forecast-deployment-frequency-with-value-stream-forecasting)
- **Tier**: Freely available for Ultimate for a limited timeIn the future, will require Ultimate with [GitLab Duo Enterprise](../subscriptions/subscription-add-ons.md)
- **Offering**: GitLab.com, Self-managed, GitLab Dedicated
- **Offering_value**: 7
- **Status**: Experiment

## Feature Block
- **Goal**: Processes and responds to your questions about your application's usage data.
- **Feature**: [Product Analytics](analytics/analytics_dashboards.md#generate-a-custom-visualization-with-gitlab-duo)
- **Tier**: Freely available for Ultimate for a limited timeIn the future, will require Ultimate with [GitLab Duo Enterprise](../subscriptions/subscription-add-ons.md)
- **Offering**: GitLab.com
- **Offering_value**: 1
- **Status**: Experiment

