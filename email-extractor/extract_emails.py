import argparse
import re
import tld
import tldextract

# Create an argument parser to accept the input file name as a command-line argument
parser = argparse.ArgumentParser()
parser.add_argument('input_file', help='the name of the input file')
args = parser.parse_args()

# Set the output file name based on the input file name
output_file = args.input_file.replace('.txt', '_unique.txt')

# Create an empty list to store the email addresses
emails = []

# Debug Vartiables
TLD_set = set()
Domain_set = set()

# Open the input file and read its contents into a string
with open(args.input_file, 'r') as file:
    text = file.read()

# Use a regular expression to find all email addresses with valid top-level domains
# The regular expression pattern matches email addresses in the format local_part@domain
# where the domain has a valid top-level domain, e.g. .com, .org, .edu, etc.
# The pattern is not case sensitive, so it will match email addresses with mixed-case local parts and domains
pattern = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}\b'
matches = re.findall(pattern, text)

# Iterate through the matches and add each email address to the list of unique emails
for match in matches:
    email = match.lower() # convert email address to lowercase to remove case sensitivity
    domain = tldextract.extract(email).domain # extract the domain from the email address
    tldomain = tldextract.extract(email).suffix # get the top-level domain for the email address
    if domain and tld.is_tld(tldomain) and 'gitlab' not in domain:        # Check if the top-level domain is valid using tldextract
        print(f'{email} - Domain : {domain} - TLD : {tldomain}')
        TLD_set.add(tldomain) # add the top-level domain to the set
        Domain_set.add(domain) # add the domain to the set of unique domains
        if email not in emails:
        # Add the email address to the list of unique emails if it is not already and no gitlab one
            emails.append(email)

# Debug set prints
print(f' contained tlds : {TLD_set}')
print(f' contained domains : {Domain_set}')

# Sort the list of unique emails in alphabetical order
emails.sort()

# Write the list of unique emails to the output file
with open(output_file, 'w') as file:
    for email in emails:
        file.write(email + '\n')
